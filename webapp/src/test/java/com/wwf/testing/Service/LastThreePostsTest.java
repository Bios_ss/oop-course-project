package com.wwf.testing.Service;

import com.wwf.entity.facePart.Item;
import com.wwf.entity.facePart.Post;
import com.wwf.service.ItemService;
import com.wwf.service.PostService;
import org.junit.Test;

import java.util.List;

/**
 * Created by Владелец on 25.04.2016.
 */
public class LastThreePostsTest {
    PostService postService = new PostService();

    @Test
    public void addTest() throws Exception {
        List<Post> allPosts = postService.getAll();
        List<Post> lastPosts = postService.getLastThree();

        //out
        System.out.println("OUT: ");
        for (int i = 0; i < allPosts.size(); i++) {
            System.out.println("post " + i + " " + allPosts.get(i));
        }

        System.out.println("======================");

        for (int i = 0; i < lastPosts.size(); i++) {
            System.out.println("post " + i + " " + lastPosts.get(i));
        }

        //WORKING PROCESS
        System.out.println("---------------------\nworking:");
        for (int j = 0; j < 3; j++) {
            for (int i = 0; i < allPosts.size(); i++) {
                if (allPosts.get(i).getId() == lastPosts.get(j).getId()) {
                    System.out.println("----\n" + allPosts.get(i) + "\n" + lastPosts.get(j) + "\n-------------");
                    allPosts.remove(i);
                    System.out.println("deleted " + i + " " + allPosts.get(i));
                }
            }
        }


//        System.out.println("======================");
//
//        for (int i = 0; i < lastPosts.size(); i++) {
//            System.out.println("post " + i + " " + lastPosts.get(i));
//        }
        System.out.println("***");
        for (int i = 0; i < allPosts.size(); i++) {
            System.out.println("post " + i + " " + allPosts.get(i));
        }
    }


}
