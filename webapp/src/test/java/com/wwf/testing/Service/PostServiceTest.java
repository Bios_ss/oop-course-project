package com.wwf.testing.Service;

import com.wwf.entity.facePart.Item;
import com.wwf.entity.facePart.Post;
import com.wwf.service.ItemService;
import com.wwf.service.PostService;
import org.junit.Test;

/**
 * Created by Владелец on 25.04.2016.
 */
public class PostServiceTest {
    PostService postService = new PostService();

    @Test
    public void addTest() throws Exception {
        System.out.println("Add full row: ");
        postService.add(new Post("New title", "Some info"));
        System.out.println("The row added successfully");

        System.out.println("Add row with empty field: ");
        postService.add(new Post("", "Some info 2"));

        System.out.println("If you isn't see something unusual it mean that everything OK");
    }

    @Test
    public void updateTest() throws Exception {
        System.out.println("Add full row: ");
        Post post = new Post("New title", "Some info3");
        post = postService.add(post);
        System.out.println("The row added successfully");
        post.setTitle("Edited title");
        postService.update(post);


        Post post1 = new Post("New title", "Some info4");
        System.out.println("Add full row: ");
        post1 = postService.add(post1);
        System.out.println("Add empty row");
        postService.update(new Post());
        System.out.println("If you isn't see something unusual it mean that everything OK");

        System.out.println("In database must be created 3 rows with info: info, info3, info4");


    }
}
