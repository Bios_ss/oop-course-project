package com.wwf.testing.Service;

import com.wwf.entity.facePart.Item;
import com.wwf.service.ItemService;
import org.junit.Test;

import java.util.Date;

/**
 * Created by Владелец on 25.04.2016.
 */
public class ItemServiceTest {
    ItemService itemService = new ItemService();

    @Test
    public void addTest() throws Exception {
        System.out.println("Add full row: ");
        itemService.add(new Item("New title", "Some info", "Animal info"));
        System.out.println("The row added successfully");

        System.out.println("Add row with empty field: ");
        itemService.add(new Item("", "Some info 2", "Animal info"));

        System.out.println("If you isn't see something unusual it mean that everything OK");
    }

    @Test
    public void updateTest() throws Exception {
        System.out.println("Add full row: ");
        Item item = new Item("New title", "Some info3", "Animal info");
        item = itemService.add(item);
        System.out.println("The row added successfully");
        item.setAnimalInfo("Edited info");
        itemService.update(item);


        Item item1 = new Item("New title", "Some info4", "Animal info");
        System.out.println("Add full row: ");
        item1 = itemService.add(item1);
        System.out.println("Add empty row");
        itemService.update(new Item());
        System.out.println("If you isn't see something unusual it mean that everything OK");

        System.out.println("In database must be created 3 rows with animal info: animal info, animal info3, animal info4");


    }
}
