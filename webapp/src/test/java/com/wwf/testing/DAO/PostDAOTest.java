package com.wwf.testing.DAO;

import com.wwf.dao.facePart.PostDAO;
import com.wwf.entity.facePart.Post;
import org.junit.Test;

import java.util.Date;
import java.util.List;

public class PostDAOTest {

    PostDAO service = new PostDAO();

    @Test
    public void testSaveRecord() throws Exception {
        System.out.println("Save test. Must be added 1 row: ");
        //Create post (without adding)
        Post post = new Post();
        post.setTitle("1");
        post.setInfo("2");
        post.setDateOfPost(new Date());

        //Add it to DB
        Post postAdding = service.add(post);

        //Вывели записанную в БД запись
        System.out.println(postAdding);

    }
    @Test
    public void testDeleteRecord() throws Exception {
        System.out.println("(Delete test)");
        //Create post (without adding)
        Post post = new Post();
        post.setTitle("1");
        post.setInfo("2");
        post.setDateOfPost(new Date());

        //Add it to DB
        post = service.add(post);

        //Delete it from DB
        service.delete(post.getId());
        System.out.println(post);
    }
    @Test
    public void testSelect() throws Exception {
        System.out.println("Select test. Must show 1 row from object, and 1 row from database (2 rows):");
        //Create post (without adding)
        Post post = new Post();
        post.setTitle("Some text");
        post.setInfo("More texts");
        post.setDateOfPost(new Date());

        //Add it to DB
        Post postAdding = service.add(post);

        //Output post to the console
        System.out.println(post);

        //Select post from DB
        Post postFromDb = service.get(postAdding.getId());
        System.out.println(postFromDb);
    }

    @Test
    public void testUpdate() throws Exception {
        System.out.println("Update test. Must be updated 1 row (1 row from object & 1 updated row from db) ");
        //Create post (without adding)
        Post post = new Post();
        post.setTitle("1");
        post.setInfo("2");
        post.setDateOfPost(new Date());

        //Add it to DB
        post = service.add(post);

        //Output post without changes
        System.out.println(post);
        //We do change
        post.setTitle("New Title");

        /*********************************************/
        String l ="";                                //
        for (int i = 0; i < 100000; i++) {           //         primitive timer LOL =)
            l+="l";                                  //
        }                                            //
        /*********************************************/
        post.setDateOfLastEdition(new Date());

        //Update post
        service.update(post);

        //Select updated post
        Post postAdding2 = service.get(post.getId());
        System.out.println(postAdding2);
    }

    @Test
    public void testGetAll() throws Exception {
        System.out.println("Select ALL test. Must be selected 3 rows: ");

        Post post = new Post();
        post.setTitle("1");
        post.setInfo("1");
        post.setDateOfPost(new Date());

        Post post2 = new Post();
        post2.setTitle("2");
        post2.setInfo("2");
        post2.setDateOfPost(new Date());

        Post post3 = new Post();
        post3.setTitle("3");
        post3.setInfo("3");
        post3.setDateOfPost(new Date());

        //Save all posts
        service.add(post);
        service.add(post2);
        service.add(post3);

        //Select all posts from DB
        List<Post> cars = service.getAll();

        //Output it into console
        for(Post c : cars){
            System.out.println(c);
        }
    }
}
