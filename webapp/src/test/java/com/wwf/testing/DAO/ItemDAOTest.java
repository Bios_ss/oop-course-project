package com.wwf.testing.DAO;

import com.wwf.dao.facePart.ItemDAO;
import com.wwf.entity.facePart.Item;
import org.junit.Test;

import java.util.Date;
import java.util.List;

public class ItemDAOTest {

    ItemDAO service = new ItemDAO();

    @Test
    public void testAddItem() throws Exception {
        System.out.println("Save test. Must be added 1 row: ");
        //Create Item (without adding)
        Item item = new Item();
        item.setToyTitle("Toy title - test addItem item");
        item.setToyInfo("Toy info");
        item.setAnimalInfo("Wild animal");
        item.setDateOfPost(new Date());

        //Add it to DB
        Item itemAdding = service.add(item);

        //Output it into console
        System.out.println(itemAdding);

    }
    @Test
    public void testDeleteItem() throws Exception {
        Item item = new Item();
        item.setToyTitle("Toy title - test delete item");
        item.setToyInfo("Toy info");
        item.setAnimalInfo("Wild animal");
        item.setDateOfPost(new Date());

        //Add it to DB
        item = service.add(item);

        //Delete it from DB
        service.delete(item.getId());
    }
    @Test
    public void testSelectItemById() throws Exception {
        System.out.println("Select test. Must show 1 row from object, and 1 row from database (2 rows):");
        Item item = new Item();
        item.setToyTitle("Toy title - test addItem item");
        item.setToyInfo("Toy info");
        item.setAnimalInfo("Wild animal");
        item.setDateOfPost(new Date());

        //Add it to DB
        Item itemAdding = service.add(item);

        //Output item to the console
        System.out.println(item);

        //Select item from DB
        Item itemFromDb = service.get(itemAdding.getId());
        System.out.println(itemFromDb);
    }

    @Test
    public void testUpdateItem() throws Exception {
        System.out.println("Update test. Must be updated 1 row (1 row from object & 1 updated row from db) ");
        Item item = new Item();
        item.setToyTitle("Toy title - test addItem item");
        item.setToyInfo("Toy info");
        item.setAnimalInfo("Wild animal");
        item.setDateOfPost(new Date());

        //Add it to DB
        item = service.add(item);

        //Output item without changes
        System.out.println(item);
        //We do change
        item.setToyTitle("New toy title");

        //Update item
        service.update(item);

        //Select updated item
        Item itemAdding2 = service.get(item.getId());
        System.out.println(itemAdding2);
    }

    @Test
    public void testGetAll() throws Exception {
        System.out.println("Select ALL test. Must be selected 3 rows: ");

        Item item = new Item();
        item.setToyTitle("Toy title 1");
        item.setToyInfo("Toy info");
        item.setAnimalInfo("Wild animal");
        item.setDateOfPost(new Date());

        Item item2 = new Item();
        item2.setToyTitle("Toy title 2");
        item2.setToyInfo("Toy info");
        item2.setAnimalInfo("Wild animal");
        item2.setDateOfPost(new Date());

        Item item3 = new Item();
        item3.setToyTitle("Toy title 3");
        item3.setToyInfo("Toy info");
        item3.setAnimalInfo("Wild animal");
        item3.setDateOfPost(new Date());
        //Save all items
        service.add(item);
        service.add(item2);
        service.add(item3);

        //Select all items from DB
        List<Item> cars = service.getAll();

        //Output it into console
        for(Item c : cars){
            System.out.println(c);
        }
    }
}