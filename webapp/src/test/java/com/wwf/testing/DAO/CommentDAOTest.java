package com.wwf.testing.DAO;

import com.wwf.dao.facePart.CommentDAO;
import com.wwf.dao.facePart.PostDAO;
import com.wwf.dao.facePart.UserDAO;
import com.wwf.entity.User;
import com.wwf.entity.facePart.Comment;
import com.wwf.entity.facePart.Post;
import org.junit.Test;

import java.util.Date;
import java.util.List;

/**
 * Created by Владелец on 24.04.2016.
 */
public class CommentDAOTest {
    CommentDAO commentService = new CommentDAO();
    UserDAO userService = new UserDAO();
    PostDAO postService = new PostDAO();

    Comment comment = new Comment();
    User user = new User();
    Post post = new Post();
    
    public void createComment() throws Exception {
        user.setUsername("First_uname");
        user.setPassword("18");
        user.setRoles("ROLE_USER");
        user.setFirstName("Adam");
        user.setLastName("Smith");
        user.setEmail("adam.smith@gmail.com");
        //Add it to DB
        user = userService.add(user);
        /*=============================================================*/
        post.setTitle("1");
        post.setInfo("2");
        post.setDateOfPost(new Date());
        //Add it to DB
        post = postService.add(post);
        /*=============================================================*/
        comment.setUser(user);
        comment.setPost(post);
        comment.setText("Some info in post");
        comment.setDate(new Date());
    }
    
    @Test
    public void testAddComment() throws Exception {
        createComment();
        System.out.println("Save test. Must be added 1 row: ");
        commentService.add(comment);
        //Output it into console
        System.out.println(comment);
    }

    @Test
    public void testDeletePurchaseHistory() throws Exception {
        createComment();
        comment = commentService.add(comment);
        commentService.delete(comment.getId());
    }

    @Test
    public void testSelectCommentById() throws Exception {
        createComment();
        System.out.println("Select test. Must show 1 row from object, and 1 row from database (2 rows):");
        comment = commentService.add(comment);
        System.out.println("Added comment:    " + comment);
        Comment commentFromDb = commentService.get(comment.getId());
        System.out.println("Selected comment: " +commentFromDb);
    }

    @Test
    public void testUpdatePurchaseHistory() throws Exception {
        createComment();
        System.out.println("Update test. Must be updated 1 row (1 row from object & 1 updated row from db) ");
        comment = commentService.add(comment);
        //Output purchaseHistory without changes
        System.out.println(comment);
        //We do change
        comment.setText("Some NEW info in post");

        /*********************************************/
        String l = "";                                //
        for (int i = 0; i < 50000; i++) {           //         primitive timer LOL =)
            l += "l";                                  //
        }                                            //
        /*********************************************/
        //Update comment
        commentService.update(comment);

        //Select updated purchaseHistory
        Comment selectedComment = commentService.get(comment.getId());
        System.out.println(selectedComment);
    }

    @Test
    public void testGetAll() throws Exception {
        System.out.println("Select ALL test. Must be selected 3 rows: ");
        //Create 3 comments and addItem it into DB
        createComment();
        commentService.add(comment);

        createComment();
        commentService.add(comment);

        createComment();
        commentService.add(comment);

        //Select all comments from DB
        List<Comment> comments = commentService.getAll();

        //Output it into console
        for (Comment c : comments) {
            System.out.println(c);
        }
    }
    @Test
    public void testGetAllCommentsByPost() throws Exception {
        System.out.println("Get All Comments By Post TEST. Must be selected 2 rows and ignored 1 row: ");
        //Create Comments
        createComment();
        commentService.add(comment);
        System.out.println(comment);

        Comment comment2 = new Comment();
        comment2.setUser(user);
        comment2.setPost(post);
        comment2.setText("Some info in post");
        comment2.setDate(new Date());
        commentService.add(comment2);
        System.out.println(comment2);

        //Create and addItem new post to DB
        Post newPost = new Post();
        newPost.setTitle("Some text");
        newPost.setInfo("More texts");
        newPost.setDateOfPost(new Date());

        //Add it to DB
        Post postAdding = postService.add(newPost);


        Comment comment3 = new Comment();
        comment3.setUser(user);
        comment3.setPost(newPost);
        comment3.setText("Some info in post");
        comment3.setDate(new Date());
        commentService.add(comment3);
        System.out.println(comment3);

        //Output it into console

        System.out.println("===========\nSelected comments:");
        System.out.println(commentService.getAllCommentsByPost(post));

    }
}
