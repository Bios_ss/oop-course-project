package com.wwf.testing.DAO;

import com.wwf.dao.facePart.ItemDAO;
import com.wwf.dao.facePart.PurchaseHistoryDAO;
import com.wwf.dao.facePart.UserDAO;
import com.wwf.entity.User;
import com.wwf.entity.facePart.Item;
import com.wwf.entity.facePart.PurchaseHistory;
import org.junit.Test;

import java.util.Date;
import java.util.List;

public class PurchaseHistoryDAOTest {

    PurchaseHistoryDAO service = new PurchaseHistoryDAO();
    ItemDAO itemService = new ItemDAO();
    UserDAO userService = new UserDAO();
    Item item;
    User user;

    public PurchaseHistoryDAOTest() throws Exception {
        createAndGetItemAndUser();
    }

    void createAndGetItemAndUser() throws Exception {
        Item item = new Item();
        item.setToyTitle("Toy title - test addItem item");
        item.setToyInfo("Toy info");
        item.setAnimalInfo("Wild animal");
        item.setDateOfPost(new Date());

        //Add it to DB
        Item itemAdding = itemService.add(item);

        //Create user (without adding)
        User user = new User();
        user.setUsername("First_uname");
        user.setPassword("18");
        user.setRoles("ROLE_USER");

        //Add it to DB
        User userAdding = userService.add(user);

        this.item = itemService.get(itemAdding.getId());
        this.user = userService.getById(userAdding.getId());

    }


    @Test
    public void testAddPurchaseHistory() throws Exception {
        System.out.println("Save test. Must be added 1 row: ");
        //Create PurchaseHistory (without adding)
        PurchaseHistory purchaseHistory = new PurchaseHistory();
        purchaseHistory.setUser(user);
        purchaseHistory.setItem(item);
        purchaseHistory.setPrice(20000);
        purchaseHistory.setAddress("20525 Mariani Avenue Coopertino CA");
        purchaseHistory.setFullName("Klots Artem");
        purchaseHistory.setComment("My comment");
        purchaseHistory.setDate(new Date());

        //Add it to DB
        PurchaseHistory purchaseHistoryAdding = service.add(purchaseHistory);

        //Output it into console
        System.out.println(purchaseHistoryAdding);

    }

    @Test
    public void testDeletePurchaseHistory() throws Exception {
        PurchaseHistory purchaseHistory = new PurchaseHistory();
        purchaseHistory.setUser(user);
        purchaseHistory.setItem(item);
        purchaseHistory.setPrice(20000);
        purchaseHistory.setDate(new Date());

        //Add it to DB
        purchaseHistory = service.add(purchaseHistory);

        //Delete it from DB
        service.delete(purchaseHistory.getId());
    }

    @Test
    public void testSelectPurchaseHistoryById() throws Exception {
        System.out.println("Select test. Must show 1 row from object, and 1 row from database (2 rows):");
        PurchaseHistory purchaseHistory = new PurchaseHistory();
        purchaseHistory.setUser(user);
        purchaseHistory.setItem(item);
        purchaseHistory.setPrice(20000);
        purchaseHistory.setDate(new Date());

        //Add it to DB
        PurchaseHistory purchaseHistoryAdding = service.add(purchaseHistory);

        //Output purchaseHistory to the console
        System.out.println(purchaseHistory);

        //Select purchaseHistory from DB
        PurchaseHistory purchaseHistoryFromDb = service.get(purchaseHistoryAdding.getId());
        System.out.println(purchaseHistoryFromDb);
    }

    @Test
    public void testUpdatePurchaseHistory() throws Exception {
        System.out.println("Update test. Must be updated 1 row (1 row from object & 1 updated row from db) ");
        PurchaseHistory purchaseHistory = new PurchaseHistory();
        purchaseHistory.setUser(user);
        purchaseHistory.setItem(item);
        purchaseHistory.setPrice(20000);
        purchaseHistory.setDate(new Date());

        //Add it to DB
        purchaseHistory = service.add(purchaseHistory);

        //Output purchaseHistory without changes
        System.out.println(purchaseHistory);
        //We do change
        purchaseHistory.setPrice(25000);

        /*********************************************/
        String l = "";                                //
        for (int i = 0; i < 100000; i++) {           //         primitive timer LOL =)
            l += "l";                                  //
        }                                            //
        /*********************************************/
        //Update purchaseHistory
        service.update(purchaseHistory);

        //Select updated purchaseHistory
        PurchaseHistory purchaseHistoryAdding2 = service.get(purchaseHistory.getId());
        System.out.println(purchaseHistoryAdding2);
    }

    @Test
    public void testGetAll() throws Exception {
        System.out.println("Select ALL test. Must be selected 3 rows: ");

        PurchaseHistory purchaseHistory = new PurchaseHistory();
        purchaseHistory.setUser(user);
        purchaseHistory.setItem(item);
        purchaseHistory.setPrice(10000);
        purchaseHistory.setDate(new Date());

        createAndGetItemAndUser();
        PurchaseHistory purchaseHistory2 = new PurchaseHistory();
        purchaseHistory2.setUser(user);
        purchaseHistory2.setItem(item);
        purchaseHistory2.setPrice(20000);
        purchaseHistory2.setDate(new Date());

        createAndGetItemAndUser();
        PurchaseHistory purchaseHistory3 = new PurchaseHistory();
        purchaseHistory3.setUser(user);
        purchaseHistory3.setItem(item);
        purchaseHistory3.setPrice(30000);
        purchaseHistory3.setDate(new Date());
        //Save all purchaseHistories

        service.add(purchaseHistory);
        service.add(purchaseHistory2);
        service.add(purchaseHistory3);

        //Select all purchaseHistory items from DB
        List<PurchaseHistory> historyItems = service.getAll();

        //Output it into console
        for (PurchaseHistory c : historyItems) {
            System.out.println(c);
        }
    }

    @Test
    public void testGetAllPurchasesByUser() throws Exception {
        System.out.println("Get All Purchases By User TEST. Must be selected 2 rows and ignored i row: ");
        //Create PurchaseHistory (without adding)
        PurchaseHistory purchaseHistory = new PurchaseHistory();
        purchaseHistory.setUser(user);
        purchaseHistory.setItem(item);
        purchaseHistory.setPrice(20000);
        purchaseHistory.setDate(new Date());

        //Add it to DB
        PurchaseHistory purchaseHistoryAdding = service.add(purchaseHistory);

        PurchaseHistory purchaseHistory2 = new PurchaseHistory();
        purchaseHistory2.setUser(user);
        purchaseHistory2.setItem(item);
        purchaseHistory2.setPrice(20000);
        purchaseHistory2.setDate(new Date());

        //Add new user to DB
        PurchaseHistory purchaseHistoryAdding2 = service.add(purchaseHistory2);

        User newUser = new User();
        newUser.setUsername("Second_uname");
        newUser.setPassword("18");
        newUser.setRoles("Something");

        //Add it to DB
        User userAdding = userService.add(newUser);

        PurchaseHistory purchaseHistory3 = new PurchaseHistory();
        purchaseHistory3.setUser(userService.getByUsername("Second_uname"));
        purchaseHistory3.setItem(item);
        purchaseHistory3.setPrice(20000);
        purchaseHistory3.setDate(new Date());
        PurchaseHistory purchaseHistoryAdding3 = service.add(purchaseHistory3);

        //Output it into console
        System.out.println("===========\nAll purchases:");
        System.out.println(purchaseHistoryAdding);
        System.out.println(purchaseHistoryAdding2);
        System.out.println(purchaseHistoryAdding3);

        System.out.println("===========\nSelected purchases:");
        System.out.println(service.getAllPurchasesByUser(user));

    }
}