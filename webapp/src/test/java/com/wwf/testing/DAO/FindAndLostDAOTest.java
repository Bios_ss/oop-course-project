package com.wwf.testing.DAO;

import com.wwf.dao.findAndLost.FindAndLostDAO;
import com.wwf.entity.findAndLost.FindAndLost;
import org.junit.Test;

import java.util.Date;
import java.util.List;

/**
 * Created by Владелец on 24.04.2016.
 */
public class FindAndLostDAOTest {
    FindAndLostDAO findAndLostService = new FindAndLostDAO();
    FindAndLost findAndLost = new FindAndLost();

    public void createFindAndLostItem(){
        findAndLost.setTitle("Lost dog");
        findAndLost.setAnimalInfo("Black dog");
        findAndLost.setContacts("my_mail@mail.ua");
        findAndLost.setFindOrLost(true);
        findAndLost.setStatus(true);
        findAndLost.setDate(new Date());
    }

    @Test
    public void testAddFindOrLost() throws Exception {
        createFindAndLostItem();
        System.out.println("Save test. Must be added 1 row: ");
        findAndLostService.add(findAndLost);
        //Output it into console
        System.out.println(findAndLost);
    }
    @Test
    public void testDeleteFindAndLost() throws Exception {
        createFindAndLostItem();
        System.out.println(findAndLost);
        //Add it to DB
        findAndLost = findAndLostService.add(findAndLost);
        //Delete it from DB
        findAndLostService.delete(findAndLost.getId());
    }

    @Test
    public void testSelectFindAndLost() throws Exception {
        System.out.println("Select test. Must show 1 row from object, and 1 row from database (2 rows):");
        createFindAndLostItem();

        //Add it to DB
        findAndLost = findAndLostService.add(findAndLost);

        //Output purchaseHistory to the console
        System.out.println(findAndLost);

        //Select purchaseHistory from DB
        FindAndLost findAndLostFromDb = findAndLostService.get(findAndLost.getId());
        System.out.println(findAndLostFromDb);
    }

    @Test
    public void testUpdateFindAndLost() throws Exception {
        System.out.println("Update test. Must be updated 1 row (1 row from object & 1 updated row from db) ");
        createFindAndLostItem();
        findAndLost = findAndLostService.add(findAndLost);

        //Output purchaseHistory without changes
        System.out.println(findAndLost);
        //We do change
        findAndLost.setContacts("my_new_mail@mail.ua");

        /*********************************************/
        String l = "";                                //
        for (int i = 0; i < 100000; i++) {           //         primitive timer LOL =)
            l += "l";                                  //
        }                                            //
        /*********************************************/
        //Update purchaseHistory
        findAndLostService.update(findAndLost);

        //Select updated purchaseHistory
        FindAndLost findAndLostFromDB = findAndLostService.get(findAndLost.getId());
        System.out.println(findAndLostFromDB);
    }
    @Test
    public void testGetSomething() {
        System.out.println("Select ALL test. Must be selected 3 rows: ");

        FindAndLost findAndLost = new FindAndLost();
        findAndLost.setTitle("Lost dog");
        findAndLost.setAnimalInfo("Black dog");
        findAndLost.setContacts("my_mail@mail.ua");
        findAndLost.setFindOrLost(true);
        findAndLost.setStatus(true);
        findAndLost.setDate(new Date());

        FindAndLost findAndLost2 = new FindAndLost();
        findAndLost2.setTitle("Lost dog");
        findAndLost2.setAnimalInfo("Black dog");
        findAndLost2.setContacts("my_mail@mail.ua");
        findAndLost2.setFindOrLost(true);
        findAndLost2.setStatus(true);
        findAndLost2.setDate(new Date());

        FindAndLost findAndLost3 = new FindAndLost();
        findAndLost3.setTitle("Lost dog");
        findAndLost3.setAnimalInfo("Black dog");
        findAndLost3.setContacts("my_mail@mail.ua");
        findAndLost3.setFindOrLost(true);
        findAndLost3.setStatus(false);
        findAndLost3.setDate(new Date());

        FindAndLost findAndLost4 = new FindAndLost();
        findAndLost4.setTitle("Lost dog");
        findAndLost4.setAnimalInfo("Black dog");
        findAndLost4.setContacts("my_mail@mail.ua");
        findAndLost4.setFindOrLost(true);
        findAndLost4.setStatus(false);
        findAndLost4.setDate(new Date());

        FindAndLost findAndLost5 = new FindAndLost();
        findAndLost5.setTitle("Lost dog");
        findAndLost5.setAnimalInfo("Black dog");
        findAndLost5.setContacts("my_mail@mail.ua");
        findAndLost5.setFindOrLost(false);
        findAndLost5.setStatus(true);
        findAndLost5.setDate(new Date());

        FindAndLost findAndLost6 = new FindAndLost();
        findAndLost6.setTitle("Lost dog");
        findAndLost6.setAnimalInfo("Black dog");
        findAndLost6.setContacts("my_mail@mail.ua");
        findAndLost6.setFindOrLost(true);
        findAndLost6.setStatus(true);
        findAndLost6.setDate(new Date());

        FindAndLost findAndLost7 = new FindAndLost();
        findAndLost7.setTitle("Lost dog");
        findAndLost7.setAnimalInfo("Black dog");
        findAndLost7.setContacts("my_mail@mail.ua");
        findAndLost7.setFindOrLost(false);
        findAndLost7.setStatus(true);
        findAndLost7.setDate(new Date());

        FindAndLost findAndLost8 = new FindAndLost();
        findAndLost8.setTitle("Lost dog");
        findAndLost8.setAnimalInfo("Black dog");
        findAndLost8.setContacts("my_mail@mail.ua");
        findAndLost8.setFindOrLost(false);
        findAndLost8.setStatus(true);
        findAndLost8.setDate(new Date());

        findAndLostService.add(findAndLost);
        findAndLostService.add(findAndLost2);
        findAndLostService.add(findAndLost3);
        findAndLostService.add(findAndLost4);
        findAndLostService.add(findAndLost5);
        findAndLostService.add(findAndLost6);
        findAndLostService.add(findAndLost7);
        findAndLostService.add(findAndLost8);

        //Select all purchaseHistory items from DB
        List<FindAndLost> getAll = findAndLostService.getAll();
        System.out.println("Get all");

        //Output it into console
        for (FindAndLost c : getAll) {
            System.out.println(c);
        }

        //Select all purchaseHistory items from DB
        List<FindAndLost> getFind = findAndLostService.getFind();
        System.out.println("Get find");

        //Output it into console
        for (FindAndLost c : getFind) {
            System.out.println(c);
        }
        //Select all purchaseHistory items from DB
        List<FindAndLost> getLost = findAndLostService.getLost();
        System.out.println("Get lost");

        //Output it into console
        for (FindAndLost c : getLost) {
            System.out.println(c);
        }
        //Select all purchaseHistory items from DB
        List<FindAndLost> getClose = findAndLostService.getCloseStatus();
        System.out.println("Get close");

        //Output it into console
        for (FindAndLost c : getClose) {
            System.out.println(c);
        }

        //Select all purchaseHistory items from DB
        List<FindAndLost> getOpen = findAndLostService.getOpenStatus();
        System.out.println("Get open");

        //Output it into console
        for (FindAndLost c : getOpen) {
            System.out.println(c);
        }
    }
}
