package com.wwf.testing.DAO;

import com.wwf.dao.facePart.UserDAO;
import com.wwf.entity.User;
import com.wwf.service.user.UserManager;
import org.junit.Test;

import javax.persistence.NoResultException;

public class UserDAOTest {

    UserDAO service = new UserDAO();
    UserManager userManager = new UserManager();

    public void testIsFinished(){
        System.out.println("The test is finished! \n ========= \n");
    }

    @Test
    public void testSaveUser() throws Exception {
        System.out.println("Save test. Must be added 1 row: ");
        //Create user (without adding)
        User user = new User();
        user.setUsername("First_uname");
        user.setPassword("18");
        user.setRoles("Something");
        user.setFirstName("Adam");
        user.setLastName("Smith");
        user.setEmail("adam.smith@gmail.com");

        //Add it to DB
        User userAdding = service.add(user);

        //Out
        System.out.println(userAdding);
        testIsFinished();
    }

    @Test
    public void testSelectUserByUsername() throws Exception {
        System.out.println("Test Select User By Username. Must be selected 1 row: ");
        //Create user (without adding)
        User user = new User();
        user.setUsername("First_uname");
        user.setPassword("18");
        user.setRoles("ROLE_USER");

        //Add it to DB
        User userAdding = service.add(user);
        User user2 = new User();
        user2.setUsername("Second_uname");
        user2.setPassword("18");
        user2.setRoles("ROLE_USER, ROLE_ADMIN");

        //Add it to DB
        User userAdding2 = service.add(user2);

        //Out
        System.out.println(userAdding);
        System.out.println(userAdding2);

        User userFromDb = service.getByUsername("First_uname");
        System.out.println(userFromDb.toString());
        testIsFinished();
    }

    @Test
    public void logInTest() throws Exception {
        System.out.println("Log in test: ");
        //Create user (without adding)
        User user = new User();
        user.setUsername("test");
        user.setPassword("18");
        user.setRoles("ROLE_USER, ROLE_ADMIN");

        //Add it to DB
        User userAdding = service.add(user);

        User user2 = new User();
        user2.setUsername("Second_uname");
        user2.setPassword("18");
        user2.setRoles("ROLE_USER");

        //Add it to DB
        User userAdding2 = service.add(user2);

        //Out
        System.out.println(userAdding);
        System.out.println(userAdding2);

        System.out.println(userManager.getUser("test"));


//        User postFromDb = service.getByUsername("First_uname");
//        System.out.println(postFromDb.toString());
        testIsFinished();
    }
    @Test
    public void testDeleteRecord() throws Exception {
        System.out.println("(Delete test)");
        User user = new User();
        user.setUsername("test");
        user.setPassword("18");
        user.setRoles("ROLE_USER, ROLE_ADMIN");

        //Add it to DB
        User userAdding = service.add(user);

        //Delete it from DB

        User userFromDb = service.getByUsername("test");
        System.out.println(userFromDb);
        service.delete(userFromDb.getId());
//        System.out.println(service.getByUsername("test"));
        testIsFinished();
    }

    @Test
    public void findEmptyUser() throws Exception {
        try {
            User unreal_user = service.getByUsername("unreal_user");
            System.out.println(unreal_user);
        }catch (NoResultException e){
            System.out.println("User is not in database");
        }
//        System.out.println(service.getByUsername("test"));
        testIsFinished();
    }
}
