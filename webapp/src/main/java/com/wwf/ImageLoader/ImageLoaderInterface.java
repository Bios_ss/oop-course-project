package com.wwf.ImageLoader;

import com.wwf.entity.User;
import com.wwf.entity.facePart.Item;
import com.wwf.entity.facePart.Post;
import com.wwf.entity.findAndLost.FindAndLost;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Владелец on 12.05.2016.
 */
public interface ImageLoaderInterface {
    public String saveImage(MultipartFile file, Post post );
    public String saveImage(MultipartFile file, Item item);
    public String saveImage(MultipartFile file, User user);
    public String saveImage(MultipartFile file, FindAndLost findAndLost);
}
