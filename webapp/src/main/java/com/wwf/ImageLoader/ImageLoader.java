package com.wwf.ImageLoader;

import com.wwf.entity.User;
import com.wwf.entity.facePart.Item;
import com.wwf.entity.facePart.Post;
import com.wwf.entity.findAndLost.FindAndLost;
import org.apache.commons.io.FileUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * Created by Владелец on 12.05.2016.
 */
public class ImageLoader implements ImageLoaderInterface {
    private String serverPath = "D:\\Cloud@Mail.Ru\\2 курс 4 семестр\\ООП\\OOP-course-project\\oop-course-project\\webapp\\target\\webapp\\resources\\images\\";
    private String projectPath = "D:\\Cloud@Mail.Ru\\2 курс 4 семестр\\ООП\\OOP-course-project\\oop-course-project\\webapp\\src\\main\\webapp\\resources\\images\\";

    private String save(String path, MultipartFile file){

        if (!file.isEmpty()) {
            File fileForSaving = new File(path);                              //create empty file
            try {
                FileUtils.writeByteArrayToFile(fileForSaving, file.getBytes());//write image into this file
            } catch (IOException e) {
                e.printStackTrace();
            }
            return fileForSaving.getName();
        } else {
            return "default.png";
        }
    }

    private String savingProcess(String sufix, long id, MultipartFile file) {
        serverPath += sufix + id + ".png";
        projectPath += sufix + id + ".png";
        System.out.println(serverPath + "\n" + projectPath + "\n" + file);
        save(projectPath, file);
        return save(serverPath, file);
    }



    @Override
    public String saveImage(MultipartFile file, Post post) {
//        serverPath += "post\\" + post.getId() + ".png";
        return savingProcess("post\\", post.getId(), file);
    }

    @Override
    public String saveImage(MultipartFile file, Item item) {
//        serverPath += "item\\" + item.getId() + ".png";
        return savingProcess("item\\", item.getId(), file);
    }

    @Override
    public String saveImage(MultipartFile file, User user) {
//        serverPath += "avatar\\" + user.getId() + ".png";

        return savingProcess("avatar\\", user.getId(), file);
    }

    @Override
    public String saveImage(MultipartFile file, FindAndLost findAndLost) {
//        serverPath += "findAndLost\\" + findAndLost.getId() + ".png";
        return savingProcess("findAndLost\\", findAndLost.getId(), file);
    }
}
