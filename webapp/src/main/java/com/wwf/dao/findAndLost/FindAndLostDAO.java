package com.wwf.dao.findAndLost;

import com.wwf.entity.facePart.Item;
import com.wwf.entity.findAndLost.FindAndLost;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Владелец on 20.04.2016.
 */
@Service
public class FindAndLostDAO {
    public EntityManager em = Persistence.createEntityManagerFactory("WWF").createEntityManager();

    public FindAndLost add(FindAndLost findAndLost) {                             //CREATE
        em.getTransaction().begin();
        FindAndLost findAndLostFromDB = em.merge(findAndLost);
        em.getTransaction().commit();
        return findAndLostFromDB;
    }

    public FindAndLost get(long id) {
        return em.find(FindAndLost.class, id);
    }    //GET (SELECT)

    public void delete(long id) {                                                 //DELETE
        em.getTransaction().begin();
        em.remove(get(id));
        em.getTransaction().commit();
    }

    public void update(FindAndLost findAndLost) {                                 //UPDATE
        em.getTransaction().begin();
        em.merge(findAndLost);
        em.getTransaction().commit();
    }

    public List<FindAndLost> getAll() {                                     //getAll (*)
        TypedQuery<FindAndLost> namedQuery = em.createNamedQuery("FindAndLost.getAll", FindAndLost.class);
        return namedQuery.getResultList();
    }

    public List<FindAndLost> getFind() {                                     //getFind (*)
        TypedQuery<FindAndLost> namedQuery = em.createNamedQuery("FindAndLost.getFind", FindAndLost.class);
        return namedQuery.getResultList();
    }

    public List<FindAndLost> getLost() {                                     //getLost (*)
        TypedQuery<FindAndLost> namedQuery = em.createNamedQuery("FindAndLost.getLost", FindAndLost.class);
        return namedQuery.getResultList();
    }

    public List<FindAndLost> getOpenStatus() {                                     //getOpenStatus (*)
        TypedQuery<FindAndLost> namedQuery = em.createNamedQuery("FindAndLost.getOpenStatus", FindAndLost.class);
        return namedQuery.getResultList();
    }

    public List<FindAndLost> getCloseStatus() {                                     //getCloseStatus (*)
        TypedQuery<FindAndLost> namedQuery = em.createNamedQuery("FindAndLost.getCloseStatus", FindAndLost.class);
        return namedQuery.getResultList();
    }
}
