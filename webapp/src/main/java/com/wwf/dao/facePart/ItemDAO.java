package com.wwf.dao.facePart;

import com.wwf.entity.facePart.Item;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Владелец on 20.04.2016.
 */
@Service
public class ItemDAO {
    public EntityManager em = Persistence.createEntityManagerFactory("WWF").createEntityManager();

    public Item add(Item item) throws Exception {                                     //CREATE
        em.getTransaction().begin();
        Item itemFromDB = em.merge(item);
        em.getTransaction().commit();
        return itemFromDB;
    }

    public Item get(long id) {
        return em.find(Item.class, id);
    }    //GET (SELECT)

    public void delete(long id) {
        em.getTransaction().begin();
        em.remove(get(id));
        em.getTransaction().commit();
    }            //DELETE

    public void update(Item item) throws Exception {                                 //UPDATE
        em.getTransaction().begin();
        em.merge(item);
        em.getTransaction().commit();
    }

    public List<Item> getAll() {                                     //GET ALL (*)
        TypedQuery<Item> namedQuery = em.createNamedQuery("Item.getAll", Item.class);
        return namedQuery.getResultList();
    }

}
