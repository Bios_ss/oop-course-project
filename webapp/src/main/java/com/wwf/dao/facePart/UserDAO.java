package com.wwf.dao.facePart;

import com.wwf.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Владелец on 18.04.2016.
 */
public class UserDAO {
    public EntityManager em = Persistence.createEntityManagerFactory("WWF").createEntityManager();

    public User add(User user) {                                     //CREATE
        em.getTransaction().begin();
        User userFromDB = em.merge(user);
        em.getTransaction().commit();
        return userFromDB;
    }

    public User getById(long id) {
        return em.find(User.class, id);
    }    //GET (SELECT)

    public User getByUsername(String username) {
        TypedQuery<User> user = em.createNamedQuery("User.getByName", User.class);
        user.setParameter("name", username);
        return new User(user.getSingleResult().getId(), user.getSingleResult().getUsername(),
                user.getSingleResult().getPassword(), user.getSingleResult().getFirstName(), user.getSingleResult().getLastName(),
                user.getSingleResult().getEmail(),user.getSingleResult().getImage(), user.getSingleResult().getRole());
    }    //GET (SELECT)


    public void delete(long id) {
        em.getTransaction().begin();
        em.remove(getById(id));
        em.getTransaction().commit();
    }            //DELETE

    public void update(User user) {                                 //UPDATE
        em.getTransaction().begin();
        em.merge(user);
        em.getTransaction().commit();
    }

    public List<User> getAll() {                                     //GET ALL (*)
        TypedQuery<User> namedQuery = em.createNamedQuery("User.getAll", User.class); //red underscore is normal(isn`t error)
        return namedQuery.getResultList();
    }
}
