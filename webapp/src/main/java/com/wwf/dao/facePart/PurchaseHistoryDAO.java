package com.wwf.dao.facePart;

import com.wwf.entity.User;
import com.wwf.entity.facePart.Item;
import com.wwf.entity.facePart.PurchaseHistory;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Владелец on 22.04.2016.
 */
public class PurchaseHistoryDAO {
    public EntityManager em = Persistence.createEntityManagerFactory("WWF").createEntityManager();

    public PurchaseHistory add(PurchaseHistory purchaseHistory){                                     //CREATE
        em.getTransaction().begin();
        PurchaseHistory purchaseHistoryFromDB = em.merge(purchaseHistory);
        em.getTransaction().commit();
        return purchaseHistoryFromDB;
    }

    public PurchaseHistory get(long id){
        return em.find(PurchaseHistory.class, id);
    }    //GET (SELECT)

    public void delete(long id){
        em.getTransaction().begin();
        em.remove(get(id));
        em.getTransaction().commit();
    }            //DELETE

    public void update(PurchaseHistory purchaseHistory){                      //UPDATE
        em.getTransaction().begin();
        em.merge(purchaseHistory);
        em.getTransaction().commit();
    }

    public List<PurchaseHistory> getAll(){                                     //GET ALL (*)
        TypedQuery<PurchaseHistory> namedQuery = em.createNamedQuery("purchase_history.getAll", PurchaseHistory.class);
        return namedQuery.getResultList();
    }

    public List<PurchaseHistory> getAllPurchasesByUser(User user){             //GET ALL Purchases By User
        TypedQuery<PurchaseHistory> namedQuery = em.createNamedQuery("purchase_history.getAllPurchasesByUser", PurchaseHistory.class);
        namedQuery.setParameter("user",user);
        return namedQuery.getResultList();
    }

    public List<PurchaseHistory> getAllPurchasesByItem(Item item){             //GET ALL Purchases By User
        TypedQuery<PurchaseHistory> namedQuery = em.createNamedQuery("purchase_history.getAllPurchasesByItem", PurchaseHistory.class);
        namedQuery.setParameter("item",item);
        return namedQuery.getResultList();
    }
}
