package com.wwf.dao.facePart;

import com.wwf.entity.facePart.Comment;
import com.wwf.entity.facePart.Post;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Владелец on 22.04.2016.
 */
public class CommentDAO {
    public EntityManager em = Persistence.createEntityManagerFactory("WWF").createEntityManager();

    public Comment add(Comment comment) throws Exception {                                     //CREATE
        em.getTransaction().begin();
        Comment commentFromDB = em.merge(comment);
        em.getTransaction().commit();
        return commentFromDB;
    }

    public Comment get(long id){
        return em.find(Comment.class, id);
    }    //GET (SELECT)

    public void delete(long id){
        em.getTransaction().begin();
        em.remove(get(id));
        em.getTransaction().commit();
    }            //DELETE

    public void update(Comment comment) throws Exception {                      //UPDATE
        em.getTransaction().begin();
        em.merge(comment);
        em.getTransaction().commit();
    }

    public List<Comment> getAll(){                                     //GET ALL (*)
        TypedQuery<Comment> namedQuery = em.createNamedQuery("comment.getAll", Comment.class);
        return namedQuery.getResultList();
    }

    public List<Comment> getAllCommentsByPost(Post post){             //GET ALL Comments By Post
        TypedQuery<Comment> namedQuery = em.createNamedQuery("comment.getAllCommentsByPostId", Comment.class);
        namedQuery.setParameter("post", post);
        return namedQuery.getResultList();
    }
}
