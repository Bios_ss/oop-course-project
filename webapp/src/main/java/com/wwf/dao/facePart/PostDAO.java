package com.wwf.dao.facePart;

import com.wwf.entity.facePart.Post;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Владелец on 17.04.2016.
 */

public class PostDAO {
    public EntityManager em = Persistence.createEntityManagerFactory("WWF").createEntityManager();

    public Post add(Post post) throws Exception {                                     //CREATE
        em.getTransaction().begin();
        Post postFromDB = em.merge(post);
        em.getTransaction().commit();
        return postFromDB;
    }

    public Post get(long id){
        return em.find(Post.class, id);
    }    //GET (SELECT)

    public void delete(long id){
        em.getTransaction().begin();
        em.remove(get(id));
        em.getTransaction().commit();
    }            //DELETE

    public void update(Post post) throws Exception {                                 //UPDATE
        em.getTransaction().begin();
        em.merge(post);
        em.getTransaction().commit();
    }

    public List<Post> getAll(){                                     //GET ALL (*)
        TypedQuery<Post> namedQuery = em.createNamedQuery("Post.getAll", Post.class);
        return namedQuery.getResultList();
    }

    public List<Post> getLastThree(){                                     //GET LAST
        TypedQuery<Post> namedQuery = em.createNamedQuery("Post.getLastThree", Post.class);
        namedQuery.setMaxResults(3);
        return namedQuery.getResultList();
    }
}
