package com.wwf.validator;

import com.wwf.entity.facePart.PurchaseHistory;
import com.wwf.form.BuyingForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Владелец on 25.04.2016.
 */
@Component
public class PurchaseHistoryValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return PurchaseHistory.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        BuyingForm buyingForm = (BuyingForm) target;

        ValidationUtils.rejectIfEmpty(errors, "price", "Price.empty", "Price must not be empty.");
        ValidationUtils.rejectIfEmpty(errors, "fullName", "fullName.empty", "Full name must not be empty.");
        ValidationUtils.rejectIfEmpty(errors, "address", "address.empty", "Address must not be empty.");

        if (buyingForm.getPrice() < 15){
            errors.rejectValue("price","Price.toSmall", "Price you entered is too small (minimum is 15$)");
        }
    }

}
