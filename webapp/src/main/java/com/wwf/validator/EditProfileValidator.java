package com.wwf.validator;

import com.wwf.entity.User;
import com.wwf.form.EditProfileForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Владелец on 25.04.2016.
 */
@Component
public class EditProfileValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        EditProfileForm editProfileForm= (EditProfileForm) target;

        ValidationUtils.rejectIfEmpty(errors, "firstName", "firstName.empty", "First Name must not be empty.");

        ValidationUtils.rejectIfEmpty(errors, "lastName", "lastName.empty", "Last Name must not be empty.");
    }

}
