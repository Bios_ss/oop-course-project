package com.wwf.validator;

import com.wwf.entity.facePart.Comment;
import com.wwf.form.AddCommentForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Владелец on 25.04.2016.
 */
@Component
public class CommentValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Comment.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        AddCommentForm addCommentForm = (AddCommentForm) target;

        ValidationUtils.rejectIfEmpty(errors, "text", "text.empty", "Text must not be empty.");
//        ValidationUtils.rejectIfEmpty(errors, "post", "post.empty", "Post must not be empty.");
//        ValidationUtils.rejectIfEmpty(errors, "user", "user.empty", "User must not be empty.");

    }

}
