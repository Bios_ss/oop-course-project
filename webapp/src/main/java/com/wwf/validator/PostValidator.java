package com.wwf.validator;

import com.wwf.entity.facePart.Post;
import com.wwf.form.PostForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Владелец on 25.04.2016.
 */
@Component
public class PostValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Post.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        PostForm postForm = (PostForm) target;

        ValidationUtils.rejectIfEmpty(errors, "title", "title.empty", "Title must not be empty.");

        ValidationUtils.rejectIfEmpty(errors, "info", "info.empty", "Info must not be empty.");
    }

}
