package com.wwf.validator;

import com.wwf.entity.User;
import com.wwf.form.ChangePasswordForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Владелец on 25.04.2016.
 */
@Component
public class
PasswordValidator implements Validator {
    private User user;

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ChangePasswordForm changePasswordForm = (ChangePasswordForm) target;

        ValidationUtils.rejectIfEmpty(errors, "currentPassword", "currentPassword.empty", "Current Password must not be empty.");

        ValidationUtils.rejectIfEmpty(errors, "newPassword", "newPassword.empty", "newPassword must not be empty.");

        ValidationUtils.rejectIfEmpty(errors, "confirmNewPassword", "confirmNewPassword.empty", "Confirmation of new password must not be empty.");

        if (!user.getPassword().equals(changePasswordForm.getCurrentPassword())){
            errors.rejectValue("currentPassword","confirmNewPassword.wrongPassword", "Wrong password!");
        }

        if (!changePasswordForm.getNewPassword().equals(changePasswordForm.getConfirmNewPassword())){
            errors.rejectValue("confirmNewPassword","confirmNewPassword.wrongConfirmation", "Wrong confirmation!");
        }

        if (changePasswordForm.getNewPassword().length() < 6){
            errors.rejectValue("newPassword","newPassword.shortPassword", "Password must be longer than 7 symbols ");
        }
    }

    public void setUser(User user) {
        this.user = user;
    }
}
