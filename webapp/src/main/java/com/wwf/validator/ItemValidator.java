package com.wwf.validator;

import com.wwf.entity.facePart.Item;
import com.wwf.form.ItemForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Владелец on 25.04.2016.
 */
@Component
public class ItemValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Item.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ItemForm itemForm = (ItemForm) target;

        ValidationUtils.rejectIfEmpty(errors, "toyTitle", "toyTitle.empty", "Toy title must not be empty.");

        ValidationUtils.rejectIfEmpty(errors, "toyInfo", "toyInfo.empty", "Toy Info must not be empty.");

        ValidationUtils.rejectIfEmpty(errors, "animalInfo", "animalInfo.empty", "Animal info must not be empty.");
    }

}
