package com.wwf.validator;

import com.wwf.dao.facePart.UserDAO;
import com.wwf.entity.User;
import com.wwf.form.EditUserForm;
import com.wwf.form.RegistrationForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import javax.persistence.NoResultException;

/**
 * Created by Владелец on 25.04.2016.
 */
@Component
public class RegistrationValidator implements Validator {

    private boolean isUserExist(String username) {
        try {
            User unreal_user = new UserDAO().getByUsername(username);
        } catch (NoResultException e) {
            return true;
        }
        return false;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        RegistrationForm registrationForm = (RegistrationForm) target;

        if (registrationForm.getUsername().isEmpty()) {
            ValidationUtils.rejectIfEmpty(errors, "username", "username.empty", "Username must not be empty.");
        } else {
            if (!this.isUserExist(registrationForm.getUsername())) {
                errors.rejectValue("username", "username.usernameIsAlreadyInUse", "This username is already in the use, please choose another one ");
            }
        }

        if (registrationForm.getPassword().isEmpty()) {
            ValidationUtils.rejectIfEmpty(errors, "password", "password.empty", "Password must not be empty.");
        } else {
            if (registrationForm.getPassword().length() < 6) {
                errors.rejectValue("password", "password.shortPassword", "Password must be longer than 7 symbols ");
            }
        }

        if (registrationForm.getConfirmPassword().isEmpty()) {
            ValidationUtils.rejectIfEmpty(errors, "confirmPassword", "confirmPassword.empty", "Confirm password must not be empty.");
        } else {
            if (!registrationForm.getPassword().equals(registrationForm.getConfirmPassword())) {
                errors.rejectValue("confirmPassword", "confirmPassword.wrongConfirmation", "Password and confirm password is not the same");
            }
        }
    }

}
