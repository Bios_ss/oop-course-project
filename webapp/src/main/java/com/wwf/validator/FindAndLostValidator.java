package com.wwf.validator;

import com.wwf.entity.findAndLost.FindAndLost;
import com.wwf.form.FindAndLostForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Владелец on 25.04.2016.
 */
@Component
public class FindAndLostValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return FindAndLost.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        FindAndLostForm findAndLostForm = (FindAndLostForm) target;

        ValidationUtils.rejectIfEmpty(errors, "title", "title.empty", "Title must not be empty.");

        ValidationUtils.rejectIfEmpty(errors, "animalInfo", "animalInfo.empty", "Animal info must not be empty.");

        ValidationUtils.rejectIfEmpty(errors, "contacts", "contacts.empty", "Contacts must not be empty.");

        if ((findAndLostForm.getFindOrLost() > 1) || (findAndLostForm.getFindOrLost() < 0)){
            errors.rejectValue("findOrLost","Price.wrongInput", "You must choose find or lost from the list");
        }
    }

}
