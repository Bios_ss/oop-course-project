package com.wwf.entity.findAndLost;

import com.sun.istack.internal.NotNull;
import com.wwf.ImageLoader.ImageLoader;
import com.wwf.entity.User;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Владелец on 20.04.2016.
 */

@Entity
@Table(name = "find_or_lost")
@NamedQueries({
        @NamedQuery(name = "FindAndLost.getAll", query = "SELECT c from FindAndLost c ORDER BY c.date DESC"),
        @NamedQuery(name = "FindAndLost.getFind", query = "SELECT c from FindAndLost c WHERE c.findOrLost = false ORDER BY c.date DESC"),
        @NamedQuery(name = "FindAndLost.getLost", query = "SELECT c from FindAndLost c WHERE c.findOrLost = true ORDER BY c.date DESC"),
        @NamedQuery(name = "FindAndLost.getOpenStatus", query = "SELECT c from FindAndLost c WHERE c.status = true ORDER BY c.date DESC"),
        @NamedQuery(name = "FindAndLost.getCloseStatus", query = "SELECT c from FindAndLost c WHERE c.status = false ORDER BY c.date DESC")
})
public class FindAndLost implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", length = 6, nullable = false)
    private long id;

    @NotNull
    @Column(name = "title", length = 50)
    private String title;      //Title of post

    @NotNull
    @Column(name = "animal_info", length = 16777)
    private String animalInfo;      //Information about toy

    @NotNull
    @Column(name = "find_or_lost")
    private boolean findOrLost;   //0 - find; 1 - lost;

    @NotNull
    @Column(name = "contacts", length = 16777)
    private String contacts;      //Information about wild animal

    @NotNull
    @Column(name = "status")
    private boolean status;     //open or close: 0 - close; 1 - open;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date; //Date of create of post

    @NotNull
    @Column(name = "img", length = 16777)
    private String image;

    public FindAndLost() {
    }

    public FindAndLost(String title, String animalInfo, int findOrLost, String contacts, User user) {
        this.title = title;
        this.animalInfo = animalInfo;
        if (findOrLost == 1) {
            this.findOrLost = true;
        } else {
            this.findOrLost = false;
        }
        this.contacts = contacts;
        this.user = user;
        status = true;
        date = new Date();
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setTitle(String Title) {
        this.title = Title;
    }

    public String getTitle() {
        return title;

    }

    public void setAnimalInfo(String animalInfo) {
        this.animalInfo = animalInfo;
    }

    public String getAnimalInfo() {
        return animalInfo;
    }

    public boolean getFindOrLost() {
        return findOrLost;
    }

    public void setFindOrLost(int findOrLost) {
        if (findOrLost == 1) {
            this.findOrLost = true;
        } else {
            this.findOrLost = false;
        }
    }

    public void setFindOrLost(boolean findOrLost) {
        this.findOrLost = findOrLost;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean getStatus() {
        return status;
    }

    public void setDate(Date dateOfPost) {
        this.date = dateOfPost;
    }

    public Date getDate() {
        return date;
    }

    public void setImage(MultipartFile file) {
        this.image = new ImageLoader().saveImage(file, this);
    }

    public String getImage() {
        return image;
    }

    @Override
    public String toString() {
        return "Find Or Lost{ id: " + id + "; title: " + title + "; animal info: " + animalInfo + "; find or lost: " + findOrLost + "; contacts: " + contacts + "; status: " + status + "; date : " + date + "; img: " + image+ "}";
    }
}
