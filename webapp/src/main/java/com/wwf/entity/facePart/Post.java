package com.wwf.entity.facePart;

import com.sun.istack.internal.Nullable;
import com.wwf.ImageLoader.ImageLoader;
import org.apache.commons.io.FileUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Владелец on 17.04.2016.
 */
@Entity
@Table(name = "post")
@NamedQueries({
        @NamedQuery(name = "Post.getAll", query = "SELECT c from Post c ORDER BY c.dateOfPost DESC"),
        @NamedQuery(name = "Post.getLastThree", query = "SELECT c FROM Post c ORDER BY dateOfPost DESC")
})
public class Post implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "post_id", length = 6, nullable = false)
    private long postId;

    @Column(name = "title", length = 50)
    private String title;      //Title of post

    @Column(name = "info", length = 50)
    private String info;      //Information

    @Column(name = "dateOfPost")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfPost; //Date of create post

    @Nullable
    @Column(name = "date_of_last_edition")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfLastEdition; //Last edition of post

    @Column(name = "img", length = 50)
    private String image;      //Title of post

    public Post() {
    }

    public Post(String title, String info) {
        this.title = title;
        this.info = info;
        dateOfPost = new Date();
    }

    public long getId() {
        return postId;
    }

    public void setId(long post_id) {
        this.postId = post_id;
    }

    public Date getDateOfPost() {
        return dateOfPost;
    }

    public void setDateOfPost(Date dateOfPost) {
        this.dateOfPost = dateOfPost;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDateOfLastEdition(Date dateOfEdition) {
        this.dateOfLastEdition = dateOfEdition;
    }

    public Date getDateOfLastEdition() {
        return dateOfLastEdition;
    }

    public void setImage(MultipartFile file) {
        this.image = new ImageLoader().saveImage(file, this);
    }

    public String getImage() {
        return image;
    }


    @Override
    public String toString() {
        return "Post{ id: " + postId + "; title: " + title + "; info: " + info + "; img: " + image+ " date: " + dateOfPost + " date of last edition: " + dateOfLastEdition + "}";
    }


}
