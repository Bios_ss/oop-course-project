package com.wwf.entity.facePart;

import com.sun.istack.internal.NotNull;
import com.wwf.entity.User;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Владелец on 24.04.2016.
 */
@Entity
@Table(name = "comment")
@NamedQueries({
        @NamedQuery(name = "comment.getAll", query = "SELECT c from Comment c ORDER BY c.date DESC"),
        @NamedQuery(name = "comment.getAllCommentsByPostId", query = "SELECT c from Comment c WHERE c.post = :post ORDER BY c.date DESC")
})
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", length = 6, nullable = false)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "post_id", nullable = false)
    private Post post;

    public void setPost(Post post) {
        this.post = post;
    }

    public Post getPost() {
        return post;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    @NotNull
    @Column(name = "text", length = 16777)
    private String text;

    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date; //Date of create of post

    public Comment(){
    }

    public Comment(Post post, User user, String text){
        this.post = post;
        this.user = user;
        this.text = text;
        System.out.println(new StringBuilder().append(post).append(user).append(text).toString());
    }
    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "Comment Item { id: " + id + "; user id: " + user.getId() + "; post id: " + post.getId() +  " text: " + text + "; date: " + date + "}";
    }
}