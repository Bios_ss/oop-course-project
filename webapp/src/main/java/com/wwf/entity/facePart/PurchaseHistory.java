package com.wwf.entity.facePart;

import com.sun.istack.internal.NotNull;
import com.wwf.entity.User;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Владелец on 22.04.2016.
 */
@Entity
@Table(name = "purchase_history_item")
@NamedQueries({
        @NamedQuery(name = "purchase_history.getAll", query = "SELECT c from PurchaseHistory c ORDER BY c.date DESC"),
        @NamedQuery(name = "purchase_history.getAllPurchasesByUser", query = "SELECT c from PurchaseHistory c WHERE c.user = :user ORDER BY c.date DESC"),
        @NamedQuery(name = "purchase_history.getAllPurchasesByItem", query = "SELECT c from PurchaseHistory c WHERE c.item = :item ORDER BY c.date DESC")
})

public class PurchaseHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", length = 6, nullable = false)
    private long id;

    @NotNull
    @Column(name = "full_name", length = 16777)
    private String fullName;

    @NotNull
    @Column(name = "address", length = 16777)
    private String address;

    @Column(name = "comment", length = 16777)
    private String comment;

    @NotNull
    @Column(name = "price_amt", length = 6, nullable = false)
    private double price;

    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date; //Date of create of post

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "item_id", nullable = false)
    private Item item;

    public void setItem(Item item) {
        this.item = item;
    }

    public Item getItem() {
        return item;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public PurchaseHistory() {
    }

    public PurchaseHistory(double price, Item item, User user){
        this.price = price;
        this.item = item;
        this.user = user;
        date = new Date();
    }

    public PurchaseHistory(double price, Item item, User user, String fullName, String address, String comment){
        this.price = price;
        this.item = item;
        this.user = user;
        this.fullName = fullName;
        this.address = address;
        this.comment = comment;
        date = new Date();
    }

    public PurchaseHistory(double price, Item item, User user, String fullName, String address){
        this.price = price;
        this.item = item;
        this.user = user;
        this.fullName = fullName;
        this.address = address;
        date = new Date();
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "Purchase History Item { id: " + id + "; user id: " + user.getId() + /*userId +*/ "; item id: " + item.getId() + /*itemId+*/ " price: " + price + "; date: " + date + "}";
    }
}
