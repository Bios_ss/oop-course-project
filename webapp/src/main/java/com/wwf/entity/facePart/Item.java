package com.wwf.entity.facePart;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import com.wwf.ImageLoader.ImageLoader;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Владелец on 20.04.2016.
 */

@Entity
@Table(name = "item")
@NamedQuery(name = "Item.getAll", query = "SELECT c from Item c ORDER BY c.dateOfPost DESC")
public class Item implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", length = 6, nullable = false)
    private long id;

    @NotNull
    @Column(name = "toy_title", length = 50)
    private String toyTitle;      //Title of post

    @NotNull
    @Column(name = "toy_info", length = 16777)
    private String toyInfo;      //Information about toy

    @NotNull
    @Column(name = "animal_info", length = 16777)
    private String animalInfo;      //Information about wild animal

    @NotNull
    @Column(name = "date_of_post")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfPost; //Date of create of post

    @NotNull
    @Column(name = "img", length = 16777)
    private String image;

    public Item() {
    }

    public Item(String toyTitle, String toyInfo, String animalInfo) {
        this.toyTitle = toyTitle;
        this.toyInfo = toyInfo;
        this.animalInfo = animalInfo;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setDateOfPost(Date dateOfPost) {
        this.dateOfPost = dateOfPost;
    }

    public Date getDateOfPost() {
        return dateOfPost;
    }

    public void setAnimalInfo(String animalInfo) {
        this.animalInfo = animalInfo;
    }

    public String getAnimalInfo() {
        return animalInfo;
    }

    public void setToyInfo(String toyInfo) {
        this.toyInfo = toyInfo;
    }

    public String getToyInfo() {
        return toyInfo;
    }

    public void setToyTitle(String toyTitle) {
        this.toyTitle = toyTitle;
    }

    public String getToyTitle() {
        return toyTitle;
    }

    public void setImage(MultipartFile file) {
        this.image = new ImageLoader().saveImage(file, this);
    }

    public String getImage() {
        return image;
    }

    @Override
    public String toString() {
        return "Item{ id: " + id + "; toy title: " + toyTitle + "; toy info: " + toyInfo + "; animal info: " + animalInfo + "; img: " + image + "; date of post: " + dateOfPost + "}";
    }
}
