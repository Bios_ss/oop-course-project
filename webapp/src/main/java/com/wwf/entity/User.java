package com.wwf.entity;

import java.util.Collection;
import java.util.HashSet;

import com.sun.istack.internal.NotNull;
import com.wwf.ImageLoader.ImageLoader;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;

/**
 * Created by Владелец on 05.04.2016.
 */
@Entity
@Table(name = "user")
@NamedQueries({
        @NamedQuery(name = "User.getByName", query = "SELECT c from User c WHERE c.username = :name"),
        @NamedQuery(name = "User.getAll", query = "SELECT c from User c")
})
public class User implements UserDetails {
    private static final long serialVersionUID = 8266525488057072269L;

    @Column(name = "username", length = 50)
    private String username;

    @Column(name = "password", length = 50)
    private String password;

    @Column(name = "role", length = 50)
    private String role;

    @Column(name = "first_name", length = 50)
    private String firstName;

    @Column(name = "last_name", length = 50)
    private String lastName;

    @Column(name = "email", length = 50)
    private String email;

    @Column(name = "img", length = 50)
    private String image;

    @Transient
    private Collection<GrantedAuthority> authorities;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", length = 6, nullable = false)
    private long id;

    public User(String username, String password, String roles) { //WITHOUT ID
        super();
        this.username = username;
        this.password = password;
        this.setRoles(roles);
        this.role = roles;
    }

    public User(String username, String password, String firstName, String lastName, String email, String roles) { // without id and with initials
        this(username, password, roles);
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public User(long id, String username, String password, String roles) { //WITH ID
        this(username, password, roles);
        this.id = id;
    }

    public User(long id, String username, String password, String firstName, String lastName, String email, String roles) {
        this(id, username, password, roles);
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public User(long id, String username, String password, String firstName, String lastName, String email, String image, String roles) {
        this(id, username, password, firstName, lastName, email, roles);
        this.image = image;
    }

    public User() {
    }

    public void setRoles(String roles) {
        this.role = roles;
        this.authorities = new HashSet<GrantedAuthority>();
        for (final String role : roles.split(",")) {
            if (role != null && !"".equals(role.trim())) {
                GrantedAuthority grandAuthority = new GrantedAuthority() {
                    private static final long serialVersionUID = 3958183417696804555L;

                    public String getAuthority() {
                        return role.trim();
                    }
                };
                this.authorities.add(grandAuthority);
            }
        }
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    public boolean isAccountNonExpired() {
        return true;
    }

    public boolean isAccountNonLocked() {
        return true;
    }

    public boolean isCredentialsNonExpired() {
        return true;
    }

    public boolean isEnabled() {
        return true;
    }

    public void setImage(MultipartFile file) {
        this.image = new ImageLoader().saveImage(file, this);
    }

    public String getImage() {
        return image;
    }

    @Override
    public String toString() {
        return "{id: " + id + "; username: " + username + "; password: " + password + "; first name: " + firstName +
                "; last name: " + lastName + "; email: " + email + "; img: " + image + "; role: " + role + "}";
    }

    public String getShortInfo() {
        return "{id: " + id + "; username: " + username + "; first name: " + firstName +
                "; last name: " + lastName + "}";
    }
}