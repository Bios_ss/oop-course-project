package com.wwf.form;

/**
 * Created by Владелец on 26.04.2016.
 */
public class ItemForm {
    private String toyTitle;
    private String toyInfo;
    private String animalInfo;

    public void setToyTitle(String toyTitle) {
        this.toyTitle = toyTitle;
    }

    public String getToyTitle() {
        return toyTitle;
    }

    public void setToyInfo(String toyInfo) {
        this.toyInfo = toyInfo;
    }

    public String getToyInfo() {
        return toyInfo;
    }

    public void setAnimalInfo(String animalInfo) {
        this.animalInfo = animalInfo;
    }

    public String getAnimalInfo() {
        return animalInfo;
    }
}
