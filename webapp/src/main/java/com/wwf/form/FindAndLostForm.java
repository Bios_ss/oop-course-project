package com.wwf.form;

/**
 * Created by Владелец on 26.04.2016.
 */
public class FindAndLostForm {
    private String title;
    private String animalInfo;
    private int findOrLost;
    private String contacts;

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setAnimalInfo(String animalInfo) {
        this.animalInfo = animalInfo;
    }

    public String getAnimalInfo() {
        return animalInfo;
    }

    public void setFindOrLost(int findOrLost) {
        this.findOrLost = findOrLost;
    }

    public void setFindOrLost(boolean findOrLost) {
        if (findOrLost) {
            this.findOrLost = 1;
        } else {
            this.findOrLost = 0;
        }
    }

    public int getFindOrLost() {
        return findOrLost;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public String getContacts() {
        return contacts;
    }
}
