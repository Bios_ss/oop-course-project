package com.wwf.form;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Владелец on 26.04.2016.
 */
public class PostForm {
    private String title;
    private String info;
    private MultipartFile multipartFile;

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }


    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }
}
