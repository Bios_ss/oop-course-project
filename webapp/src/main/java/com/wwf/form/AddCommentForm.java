package com.wwf.form;

import com.wwf.dao.facePart.UserDAO;
import com.wwf.entity.User;
import com.wwf.entity.facePart.Post;
import com.wwf.service.PostService;

/**
 * Created by Владелец on 26.04.2016.
 */
public class AddCommentForm {
    private String text;
    private int post;
    private String user;

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setPost(int post) {
        this.post = post;
    }

    public Post getPost() {
        PostService postService = new PostService();
        return postService.get(post);
    }

    public void setUser(String user) {
        this.user = user;
    }

    public User getUser() {
        UserDAO userDAO = new UserDAO();
        return userDAO.getByUsername(user);
    }
}
