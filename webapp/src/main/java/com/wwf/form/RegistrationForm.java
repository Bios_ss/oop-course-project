package com.wwf.form;

/**
 * Created by Владелец on 26.04.2016.
 */
public class RegistrationForm extends EditUserForm{
    private String username;
    private String confirmPassword;

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }
}
