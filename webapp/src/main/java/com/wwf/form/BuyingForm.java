package com.wwf.form;

import com.wwf.entity.User;
import com.wwf.entity.facePart.Item;

/**
 * Created by Владелец on 26.04.2016.
 */
public class BuyingForm {
    private int item;
    private double price;
    private String fullName;
    private String address;
    private String comment;

    public void setItem(int item) {
        this.item = item;
    }

    public int getItem() {
        return item;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }
}
