package com.wwf.service.user;

import java.util.HashMap;

import com.wwf.dao.facePart.UserDAO;
import com.wwf.entity.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


/**
 * Created by Владелец on 05.04.2016.
 */
@Service
public class UserManager {
    private HashMap<String, User> users;
    UserDAO service = new UserDAO();


    public UserManager() {
        users = new HashMap<String, User>();
        users.put("john", new User("john", "1", "ROLE_USER"));
        users.put("bob", new User("bob", "2", "ROLE_USER, ROLE_ADMIN"));
    }

    public User getUser(String username) throws UsernameNotFoundException {
        System.out.println("getUser " + username + " => getByUsername ");
        users.put(username, service.getByUsername(username));
        System.out.println(users.get(username));

        if (!users.containsKey(username)) {
            System.out.println(username);
            throw new UsernameNotFoundException(username + " not found");
        }

        return users.get(username);
    }
}