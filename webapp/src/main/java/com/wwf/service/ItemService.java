package com.wwf.service;

import com.wwf.dao.facePart.ItemDAO;
import com.wwf.entity.facePart.Item;
import com.wwf.entity.facePart.PurchaseHistory;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;

/**
 * Created by Владелец on 25.04.2016.
 */
public class ItemService {

    public Item add(Item item) throws Exception {                                     //CREATE
        item.setDateOfPost(new Date());
        try {
            if (item.getToyTitle().isEmpty())
                throw new Exception("Toy title can't be NULL !");

            if (item.getToyInfo().isEmpty())
                throw new Exception("Toy info can't be NULL !");

            if (item.getAnimalInfo().isEmpty())
                throw new Exception("Animal info can't be NULL !");
        } catch (Exception e) {
            return item;
        }

        return new ItemDAO().add(item);
    }

    public void add(Item item, MultipartFile file) throws Exception {
        this.update(this.add(item), file);
    }

    public Item get(long id) {
        return new ItemDAO().get(id);
    }

    /**
     *
     * Safe deleting is realised by trigger in database
     * This trigger called delete_item
     *
     * Trigger logic:
     * all purchases which have deleting item in item_id will be
     * change to deleted_item (item with id 0). This Item save all
     * info about purchases, and we can save delete item, without
     * loosing info
     *
     **/
    public void delete(long id) {
        System.out.println("input id - " + id);
        new ItemDAO().delete(id);
    }

    public void update(Item item) throws Exception {                                 //UPDATE
        try {
            if (item.getToyTitle().isEmpty()) {
                throw new Exception("Toy title can't be NULL !");
            }

            if (item.getToyInfo().isEmpty()) {
                throw new Exception("Toy info can't be NULL !");
            }

            if (item.getAnimalInfo().isEmpty()) {
                throw new Exception("Animal info can't be NULL !");
            }
        } catch (Exception e) {
            return;
        }
        new ItemDAO().update(item);
    }

    public void update(Item item, MultipartFile file) throws Exception {
        item.setImage(file);
        this.update(item);
    }

    public List<Item> getAll() {
        List<Item> items = new ItemDAO().getAll();
        for (int i = 0; i < items.size(); i++) {    //remove deleted item
            if (items.get(i).getId() == 0) {
                items.remove(i);
            }
        }
        return items;
    }

    public List<Item> getRandomThree(){
        List<Item> threeItems = new ItemDAO().getAll();
        List<Item> allItems = new ItemDAO().getAll();
        threeItems.clear();
        for (int i = 0; i < 3; i++) {
            threeItems.add(allItems.get((int) (Math.random() * (allItems.size() - 1))));
        }
        return threeItems;
    }

}