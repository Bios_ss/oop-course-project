package com.wwf.service;

import com.wwf.dao.facePart.UserDAO;
import com.wwf.entity.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by Владелец on 25.04.2016.
 */
public class UserService {
    public UserService() {
    }

    public User add(User user) throws Exception {                                     //CREATE
        return new UserDAO().add(user);
    }

    public void add(User user, MultipartFile file) throws Exception {
        this.update(this.add(user), file);
    }

    public User get(long id) {
        return new UserDAO().getById(id);
    }

    public void delete(long id) {
        new UserDAO().delete(id);
    }

    public void update(User user) throws Exception {                                 //UPDATE
        new UserDAO().update(user);
    }

    public void update(User user, MultipartFile file) throws Exception {
        user.setImage(file);
        this.update(user);
    }

    public List<User> getAll() {
        return new UserDAO().getAll();
    }

    public User getByUsername(String username) {
        return new UserDAO().getByUsername(username);
    }
}