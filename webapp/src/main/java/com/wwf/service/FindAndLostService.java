package com.wwf.service;

import com.wwf.dao.findAndLost.FindAndLostDAO;
import com.wwf.entity.findAndLost.FindAndLost;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by Владелец on 05.05.2016.
 */
public class FindAndLostService {
    FindAndLostDAO findAndLostDAO = new FindAndLostDAO();

    public FindAndLost add(FindAndLost findAndLost) {
        return findAndLostDAO.add(findAndLost);
    }

    public FindAndLost add(FindAndLost findAndLost, MultipartFile file){
        return this.update(this.add(findAndLost), file);
    }

    public FindAndLost get(long id) {
        return findAndLostDAO.get(id);
    }

    public void delete(long id) {
        findAndLostDAO.delete(id);
    }

    public FindAndLost update(FindAndLost findAndLost) {
        findAndLostDAO.update(findAndLost);
        return findAndLost;
    }

    public FindAndLost update(FindAndLost findAndLost, MultipartFile file){
        findAndLost.setImage(file);
        return this.update(findAndLost);
    }

    public void closeAdvert(FindAndLost findAndLost){
        findAndLost.setStatus(false);
        findAndLostDAO.update(findAndLost);
    }

    public List<FindAndLost> getAll() {
        return findAndLostDAO.getAll();
    }

    public List<FindAndLost> getFind() {
        return findAndLostDAO.getFind();
    }

    public List<FindAndLost> getLost() {
        return findAndLostDAO.getLost();
    }

    public List<FindAndLost> getOpenStatus() {
        return findAndLostDAO.getOpenStatus();
    }

    public List<FindAndLost> getCloseStatus() {
        return findAndLostDAO.getCloseStatus();
    }


}