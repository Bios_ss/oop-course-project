package com.wwf.service;

import com.wwf.dao.facePart.PurchaseHistoryDAO;
import com.wwf.entity.User;
import com.wwf.entity.facePart.Item;
import com.wwf.entity.facePart.PurchaseHistory;

import java.util.List;

/**
 * Created by Владелец on 30.04.2016.
 *///Really Service (Not a upgraded DAO)
public class PurchaseHistoryService {
    public PurchaseHistory add(PurchaseHistory purchaseHistory){
        PurchaseHistoryDAO purchaseHistoryDAO = new PurchaseHistoryDAO();
        try {
            if (purchaseHistory.getPrice() <15)
                throw new Exception("Price cant be cheaper then 15$");
        } catch (Exception e) {
            return purchaseHistory;
        }
        return purchaseHistoryDAO.add(purchaseHistory);
    }

    public PurchaseHistory get(long id){
        PurchaseHistoryDAO purchaseHistoryDAO = new PurchaseHistoryDAO();
        return purchaseHistoryDAO.get(id);
    }

    public void delete(long id){
        PurchaseHistoryDAO purchaseHistoryDAO = new PurchaseHistoryDAO();
        purchaseHistoryDAO.delete(id);
    }

    public void update(PurchaseHistory purchaseHistory){
        PurchaseHistoryDAO purchaseHistoryDAO = new PurchaseHistoryDAO();
        try {
            if (purchaseHistory.getPrice() <15) {
                throw new Exception("Price cant be cheaper then 15$");
            }else {
//                purchaseHistoryDAO.update(purchaseHistory);
            }
        } catch (Exception e) {}
        purchaseHistoryDAO.update(purchaseHistory);
    }

    public List<PurchaseHistory> getAll(){
        PurchaseHistoryDAO purchaseHistoryDAO = new PurchaseHistoryDAO();
        return purchaseHistoryDAO.getAll();
    }

    public List<PurchaseHistory> getAllPurchasesByUser(User user){
        PurchaseHistoryDAO purchaseHistoryDAO = new PurchaseHistoryDAO();
        return purchaseHistoryDAO.getAllPurchasesByUser(user);
    }

    public List<PurchaseHistory> getAllPurchasesByItem(Item item){
        PurchaseHistoryDAO purchaseHistoryDAO = new PurchaseHistoryDAO();
        return purchaseHistoryDAO.getAllPurchasesByItem(item);
    }
}
