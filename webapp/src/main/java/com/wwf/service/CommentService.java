package com.wwf.service;

import com.wwf.dao.facePart.CommentDAO;
import com.wwf.dao.facePart.CommentDAO;
import com.wwf.entity.facePart.Comment;
import com.wwf.entity.facePart.Post;

import java.util.Date;
import java.util.List;

/**
 * Created by Владелец on 25.04.2016.
 */
public class CommentService {
    CommentDAO commentDAO = new CommentDAO();

    public Comment add(Comment comment) throws Exception {                                     //CREATE
        comment.setDate(new Date());
        try {
            if (comment.getText().isEmpty())
                throw new Exception("Text can't be NULL !");
        }catch (Exception e){
            return comment;
        }

        return commentDAO.add(comment);
    }

    public Comment get(long id){
        return commentDAO.get(id);
    }

    public void delete(long id){
        commentDAO.delete(id);
    }

    public void update(Comment comment) throws Exception {                                 //UPDATE
        try {
            if (comment.getText().isEmpty()) {
                throw new Exception("Text can't be NULL !");
            }
        }catch (Exception e ){
            return;
        }
        commentDAO.update(comment);
    }

    public List<Comment> getAll(){
        return commentDAO.getAll();
    }

    public List<Comment> getAllCommentsByPost(Post post){
        return commentDAO.getAllCommentsByPost(post);
    }

}