package com.wwf.service;

import com.wwf.dao.facePart.PostDAO;
import com.wwf.entity.facePart.Comment;
import com.wwf.entity.facePart.Post;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;

/**
 * Created by Владелец on 25.04.2016.
 */
public class PostService {

    public Post add(Post post) throws Exception {                                     //CREATE
        post.setDateOfPost(new Date());
        try {
            if (post.getTitle().isEmpty())
                throw new Exception("Title can't be NULL !");
            if (post.getInfo().isEmpty())
                throw new Exception("Info can't be NULL !");
        }catch (Exception e){
            return post;
        }
        return new PostDAO().add(post);
    }

    public void add(Post post, MultipartFile file) throws Exception {
        this.update(this.add(post),file);
    }

    public Post get(long id){
        return new PostDAO().get(id);
    }

    public void delete(long id){
        for (Comment comment : new CommentService().getAllCommentsByPost(this.get(id))) {
            new CommentService().delete(comment.getId());
        }
        new PostDAO().delete(id);
    }

    public List<Post> getAll(){
        return new PostDAO().getAll();
    }

    public List<Post> getLastThree(){
        return new PostDAO().getLastThree();
    }

    public void update(Post post) throws Exception {
        post.setDateOfLastEdition(new Date());//UPDATE
        try {
            if (post.getTitle().isEmpty()) {
                throw new Exception("Title can't be NULL !");
            }

            if (post.getInfo().isEmpty()) {
                throw new Exception("Info can't be NULL !");
            }
        }catch (Exception e ){
            return;
        }
        new PostDAO().update(post);
    }

    public void update(Post post, MultipartFile file) throws Exception {
        post.setImage(file);
        this.update(post);
    }


}