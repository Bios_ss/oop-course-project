package com.wwf.controllers;

import com.wwf.entity.facePart.Comment;
import com.wwf.form.AddCommentForm;
import com.wwf.service.CommentService;
import com.wwf.service.ItemService;
import com.wwf.service.PostService;
import com.wwf.validator.CommentValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Владелец on 27.04.2016.
 */
@Controller
@RequestMapping("/post")
public class PostController {
    @Autowired
    private CommentValidator commentValidator;

    ItemService itemService = new ItemService();
    CommentService commentService = new CommentService();
    PostService postService = new PostService();

    @RequestMapping(value = "/{post}", method = RequestMethod.GET)
    public String showPost(@PathVariable("post") int post, ModelMap model) {
        AddCommentForm addCommentForm = new AddCommentForm();
        commentService.getAllCommentsByPost(postService.get(post));
        model.put("addCommentForm", addCommentForm);
        model.put("getPost", postService.get(post));

        List <Comment> comments= commentService.getAllCommentsByPost(postService.get(post));
        for (Comment i : comments
             ) {
            i.getUser().setPassword("hidden");
        }
        model.put("getComments", comments);
        for (Comment i : comments
                ) {
            i.getUser().setPassword("hidden");
        }
        return "post/post";
    }

    @RequestMapping(value = "/{post}", method = RequestMethod.POST)
    public String processShowPost(@PathVariable("post") int post, Principal principal, AddCommentForm addCommentForm, ModelMap model,BindingResult result) throws Exception {

        commentService.getAllCommentsByPost(postService.get(post));
        model.put("addCommentForm", addCommentForm);
        model.put("getPost", postService.get(post));

        System.out.println(Arrays.toString(this.getClass().getConstructors()));

        commentValidator.validate(addCommentForm, result);
        if (result.hasErrors()) {
            return "post/post";
        }
        CommentService commentService = new CommentService();
        addCommentForm.setUser(principal.getName());
        addCommentForm.setPost(post);
//        System.out.println(new StringBuilder().append(addCommentForm.getPost()).append("/n").append(addCommentForm.getUser()).append("/n").append(addCommentForm.getText()));
        commentService.add(new Comment(addCommentForm.getPost(), addCommentForm.getUser(),addCommentForm.getText()));

        List <Comment> comments= commentService.getAllCommentsByPost(postService.get(post));
        for (Comment i : comments
                ) {
            i.getUser().setPassword("hidden");
        }
        model.put("getComments", comments);
        return "post/post";
    }

    @RequestMapping(value = "/{post}/delete/{comment}", method = RequestMethod.DELETE)
    public void deleteComment(@PathVariable("post") int post, @PathVariable("comment") int comment, Principal principal/*, AddCommentForm addCommentForm, ModelMap model,BindingResult result*/) throws Exception {
        CommentService commentService = new CommentService();
        if(commentService.get(comment).getUser().getUsername().equals(principal.getName())) {
            commentService.delete(comment);
        }
    }
}
