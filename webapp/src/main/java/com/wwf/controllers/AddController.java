package com.wwf.controllers;

import com.wwf.entity.facePart.Comment;
import com.wwf.entity.facePart.Item;
import com.wwf.entity.facePart.Post;
import com.wwf.form.AddCommentForm;
import com.wwf.form.ItemForm;
import com.wwf.form.PostForm;
import com.wwf.service.CommentService;
import com.wwf.service.ItemService;
import com.wwf.service.PostService;
import com.wwf.validator.CommentValidator;
import com.wwf.validator.ItemValidator;
import com.wwf.validator.PostValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Владелец on 25.04.2016.
 */
@Controller
@RequestMapping("/add")
public class AddController {
    @Autowired
    private ItemValidator itemValidator;

    @Autowired
    private PostValidator postValidator;

    @Autowired
    private CommentValidator commentValidator;


    @RequestMapping(value = "/item", method = RequestMethod.GET)
    public String addItem(ModelMap model) {
        ItemForm itemForm = new ItemForm();
        model.put("itemForm", itemForm);
        return "item/addItem";
    }

    @RequestMapping(value = "/item", method = RequestMethod.POST)
    public String processAddItem(@RequestParam MultipartFile file,ItemForm itemForm, BindingResult result) throws Exception {
        itemValidator.validate(itemForm, result);
        if (result.hasErrors()) {
            return "item/addItem";
        }
        new ItemService().add(new Item(itemForm.getToyTitle(), itemForm.getToyInfo(), itemForm.getAnimalInfo()),file);
        return "admin/admin";
    }

    @RequestMapping(value = "/post", method = RequestMethod.GET)
    public String addPost(ModelMap model) {
        PostForm postForm = new PostForm();
        model.put("postForm", postForm);
        return "post/addPost";
    }

    @RequestMapping(value = "/post", method = RequestMethod.POST) //add post with file
    public String processAddPost(@RequestParam MultipartFile file, PostForm postForm, BindingResult result) throws Exception {
        postValidator.validate(postForm, result);
        if (result.hasErrors()) {
            return "post/addPost";
        }
        PostService postService = new PostService();
        postService.update(postService.add(new Post(postForm.getTitle(), postForm.getInfo())), file);
        return "admin/admin";
    }

    @RequestMapping(value = "/comment", method = RequestMethod.GET)
    public String addComment(ModelMap model) {
        AddCommentForm addCommentForm = new AddCommentForm();
        model.put("addCommentForm", addCommentForm);
        return "post/addComment";
    }

    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    public String processAddPost(AddCommentForm addCommentForm, BindingResult result) throws Exception {
        commentValidator.validate(addCommentForm, result);
        if (result.hasErrors()) {
            return "post/addComment";
        }
        CommentService commentService = new CommentService();
        commentService.add(new Comment(addCommentForm.getPost(), addCommentForm.getUser(),addCommentForm.getText()));
        return "post/addComment";
    }

}
