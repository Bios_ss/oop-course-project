package com.wwf.controllers;

import com.wwf.entity.User;
import com.wwf.entity.facePart.Item;
import com.wwf.entity.facePart.Post;
import com.wwf.form.RegistrationForm;
import com.wwf.service.ItemService;
import com.wwf.service.PostService;
import com.wwf.service.UserService;
import com.wwf.validator.RegistrationValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping("/error")
public class ErrorController {
    @RequestMapping(value = "/404", method = RequestMethod.GET)
    public String addComment() {

        return "errors/404";
        }
}
