package com.wwf.controllers;

import com.wwf.entity.User;
import com.wwf.entity.findAndLost.FindAndLost;
import com.wwf.form.FindAndLostForm;
import com.wwf.service.FindAndLostService;
import com.wwf.service.UserService;
import com.wwf.validator.FindAndLostValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.security.Principal;
import java.util.List;

/**
 * Created by Владелец on 05.05.2016.
 */
@Controller
@RequestMapping("/find-and-lost")
public class FindAndLostController {
    FindAndLostService findAndLostService = new FindAndLostService();
    UserService userService = new UserService();

    @Autowired
    FindAndLostValidator findAndLostValidator = new FindAndLostValidator();

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String findAndLostIndex(ModelMap model) {
        return "findAndLost/findAndLost";
    }

    @RequestMapping(value = "/find", method = RequestMethod.GET)
    public String find(ModelMap model, Principal principal) {
        List<FindAndLost> allFind = findAndLostService.getFind();
        for (FindAndLost find : allFind) {
            if (find.getAnimalInfo().length() > 740) {
                find.setAnimalInfo(find.getAnimalInfo().substring(0, 740) + "...");
            }
        }

        User currentUser;
        //is current user have access to edit?
        try {
            currentUser = userService.getByUsername(principal.getName());
        } catch (Exception e) {
            currentUser = new User();
            currentUser.setId(-1);
        }

        model.put("currentUserId", currentUser.getId());
        model.put("find", allFind);
        return "findAndLost/find";
    }

    @RequestMapping(value = "/lost", method = RequestMethod.GET)
    public String lost(ModelMap model, Principal principal) {
        List<FindAndLost> allLost = findAndLostService.getLost();
        for (FindAndLost lost : allLost) {
            if (lost.getAnimalInfo().length() > 740) {
                lost.setAnimalInfo(lost.getAnimalInfo().substring(0, 740) + "...");
            }
        }

        User currentUser;
        //is current user have access to edit?
        try {
            currentUser = userService.getByUsername(principal.getName());
        } catch (Exception e) {
            currentUser = new User();
            currentUser.setId(-1);
        }

        model.put("currentUserId", currentUser.getId());
        model.put("lost", findAndLostService.getLost());
        return "findAndLost/lost";
    }

    @RequestMapping(value = "/details/{advert}", method = RequestMethod.GET)
    public String details(@PathVariable("advert") int advert, ModelMap model) {
        model.put("advert", findAndLostService.get(advert));
        System.out.println(findAndLostService.get(advert));
        return "findAndLost/findAndLostDetails";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addAdvert(ModelMap model) {
        model.put("findAndLostForm", new FindAndLostForm());
        return "findAndLost/addFindAndLost";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addAdvertPost(@RequestParam MultipartFile file, FindAndLostForm findAndLostForm, Model model, Principal principal, BindingResult result) throws Exception {
        System.out.println(findAndLostForm.toString());
        findAndLostValidator.validate(findAndLostForm, result);
        if (result.hasErrors()) {
            return "findAndLost/addFindAndLost";
        }
        FindAndLost findAndLost = findAndLostService.add(new FindAndLost(findAndLostForm.getTitle(), findAndLostForm.getAnimalInfo(), findAndLostForm.getFindOrLost(), findAndLostForm.getContacts(), userService.getByUsername(principal.getName())), file);
        model.addAttribute("advert", findAndLost);
        return "findAndLost/findAndLostDetails";
    }

    @RequestMapping(value = "/details/{advert}/close-advert", method = RequestMethod.GET)
    public String closeAdvert(@PathVariable("advert") int advert, Principal principal, ModelMap model/*,BindingResult result*/) throws Exception {
        if (findAndLostService.get(advert).getUser().getUsername().equals(principal.getName()) || userService.getByUsername(principal.getName()).getRole().matches("ADMIN")) {
            findAndLostService.closeAdvert(findAndLostService.get(advert));
        }
        return this.details(advert, model);
    }

    @RequestMapping(value = "/details/{advert}/edit", method = RequestMethod.GET)
    public String editAdvert(ModelMap model, @PathVariable("advert") int advert, Principal principal) {
        FindAndLostForm findAndLostForm = new FindAndLostForm();
        FindAndLost findAndLost = findAndLostService.get(advert);
        try {
            User currentUser = userService.getByUsername(principal.getName());
            if (!currentUser.getRole().equals("ROLE_USER,ROLE_ADMIN")) {
                if (findAndLost.getUser().getId() != userService.getByUsername(principal.getName()).getId()) {
                    return "errors/404";
                }
            }
        }catch (Exception e){
            return "errors/404";
        }


        findAndLostForm.setAnimalInfo(findAndLost.getAnimalInfo());
        findAndLostForm.setContacts(findAndLost.getContacts());

        model.put("findAndLostForm", findAndLostForm);
        model.put("advert", findAndLost);
        return "findAndLost/editFindAndLost";
    }

    @RequestMapping(value = "/details/{advert}/edit", method = RequestMethod.POST)
    public String editAdvertPost(@RequestParam MultipartFile file, @PathVariable("advert") int advert, FindAndLostForm findAndLostForm, Model model, Principal principal, BindingResult result) throws Exception {
        FindAndLost findAndLost = findAndLostService.get(advert);
        findAndLostForm.setFindOrLost(findAndLost.getFindOrLost());
        try {
            User currentUser = userService.getByUsername(principal.getName());
            if (!currentUser.getRole().equals("ROLE_USER,ROLE_ADMIN")) {
                if (findAndLost.getUser().getId() != userService.getByUsername(principal.getName()).getId()) {
                    return "errors/404";
                }
            }
        }catch (Exception e){
            return "errors/404";
        }
        findAndLostValidator.validate(findAndLostForm, result);
        if (result.hasErrors()) {
            return "findAndLost/editFindAndLost";
        }
        findAndLost.setAnimalInfo(findAndLostForm.getAnimalInfo());
        findAndLost.setContacts(findAndLostForm.getContacts());
        findAndLost.setTitle(findAndLostForm.getTitle());

        findAndLostService.update(findAndLost, file);

        model.addAttribute("advert", findAndLost);
        return "findAndLost/editFindAndLost";
    }
}
