package com.wwf.controllers;

import com.wwf.entity.User;
import com.wwf.entity.facePart.Item;
import com.wwf.entity.facePart.Post;
import com.wwf.form.RegistrationForm;
import com.wwf.service.ItemService;
import com.wwf.service.PostService;
import com.wwf.service.UserService;
import com.wwf.validator.RegistrationValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

    private int visitorCount = 0;
    ItemService itemService = new ItemService();
    PostService postService = new PostService();
    UserService userService = new UserService();

    @Autowired
    RegistrationValidator registrationValidator = new RegistrationValidator();

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String addComment(ModelMap model) {
        //working with posts
        List<Post> allPosts = postService.getAll();
        List<Post> lastPosts = postService.getLastThree();

        for (int j = 0; j < 3; j++) {
            for (int i = 0; i < allPosts.size(); i++) {
                if (allPosts.get(i).getId() == lastPosts.get(j).getId()) {
                    allPosts.remove(i);
                }
            }
        }

        //text limit for all posts
        for (Post post:allPosts) {
            if (post.getInfo().length() > 480) {
                post.setInfo(post.getInfo().substring(0, 480) + "...");
            }
        }

        //text limit for last 3 posts
        for (Post post:lastPosts) {
            if (post.getInfo().length() > 70) {
                post.setInfo(post.getInfo().substring(0, 70) + "...");
            }
        }

        model.put("getThreePosts", lastPosts);
        model.put("getAllPosts", allPosts);
        model.put("getAllItems", itemService.getRandomThree());
//		System.out.println(postService.getAll());
//		System.out.println(itemService.getAll());
        return "home";
    }

    @RequestMapping(value = "/items", method = RequestMethod.GET)
    public String items(ModelMap model) {
        List<Item> items = itemService.getAll();
        for (Item item : items) {
            if (item.getAnimalInfo().length() > 500) {
//                System.out.println(item.getAnimalInfo());
                item.setAnimalInfo(item.getAnimalInfo().substring(0, 500) + "...");
            }
        }
        model.put("getAllItems", items);
        return "items";
    }

    @RequestMapping("/index")
    public String index(Model model) {
        model.addAttribute("visitorCount", visitorCount++);
        return "index";
    }

    @RequestMapping("/login")
    public String login(Model model) {
        model.addAttribute("visitorCount", visitorCount++);
        return "login";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registrationGet(ModelMap model) {
        model.put("registrationForm", new RegistrationForm());
        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registrationPost(@RequestParam MultipartFile file, RegistrationForm registrationForm, BindingResult result) throws Exception {
        registrationValidator.validate(registrationForm, result);
        if (result.hasErrors()) {
            return "registration";
        }
//		if (!file.isEmpty()) {
        userService.add(new User(registrationForm.getUsername(), registrationForm.getPassword(), registrationForm.getFirstName(), registrationForm.getLastName(), registrationForm.getEmail(), "ROLE_USER"), file);
//		} else {
//			userService.add(new User(registrationForm.getUsername(),registrationForm.getPassword(),registrationForm.getFirstName(),registrationForm.getLastName(),registrationForm.getEmail(),"ROLE_USER"));
//		}
        return "successRegistration";
    }

    @RequestMapping("/bootstrap")
    public String bootstrap(Model model) {
        return "bootstrap";
    }
}
