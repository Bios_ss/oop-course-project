package com.wwf.controllers;

import com.wwf.entity.User;
import com.wwf.entity.facePart.Item;
import com.wwf.entity.facePart.Post;
import com.wwf.form.EditUserForm;
import com.wwf.form.ItemForm;
import com.wwf.form.PostForm;
import com.wwf.service.*;
import com.wwf.validator.EditUserValidator;
import com.wwf.validator.ItemValidator;
import com.wwf.validator.PostValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.security.Principal;

@Controller
@RequestMapping("/admin")
public class AdminController {
    UserService userService = new UserService();
    ItemService itemService = new ItemService();
    PurchaseHistoryService purchaseHistoryService = new PurchaseHistoryService();
    PostService postService = new PostService();

    @Autowired
    EditUserValidator editUserValidator = new EditUserValidator();

    @Autowired
    ItemValidator itemValidator = new ItemValidator();

    @Autowired
    private PostValidator postValidator;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String admin() {
        return "admin/admin";
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String getAllUsers(Model model) {
        model.addAttribute("users", userService.getAll());
        return "admin/users";
    }

    @RequestMapping(value = "/user/{username}/edit", method = RequestMethod.GET)
    public String EditUserGet(Model model, @PathVariable("username") String username) {
        model.addAttribute("user", userService.getByUsername(username));
        model.addAttribute("editUserForm", new EditUserForm());
        return "admin/editUser";
    }

    @RequestMapping(value = "/user/{username}/edit", method = RequestMethod.POST)
    public String EditUserPost(@PathVariable("username") String username, @RequestParam MultipartFile file, ModelMap model, EditUserForm editUserForm, BindingResult result) throws Exception {
//		passwordValidator.setUser(sentUser);
        editUserValidator.validate(editUserForm, result);
        if (result.hasErrors()) {
            return "admin/editUser";
        }
        User user = userService.getByUsername(username);
        user.setPassword(editUserForm.getPassword());
        user.setFirstName(editUserForm.getFirstName());
        user.setLastName(editUserForm.getLastName());
        user.setEmail(editUserForm.getEmail());
        userService.update(user, file);
        model.addAttribute("user", user);
        return "user/somebodiesProfile";
    }

    @RequestMapping(value = "/items", method = RequestMethod.GET)
    public String getAllItems(Model model) {
        model.addAttribute("items", itemService.getAll());
        return "admin/items";
    }

    @RequestMapping(value = "/item/{id}/edit", method = RequestMethod.GET)
    public String EditItemGet(Model model, @PathVariable("id") int id) {
        Item item = itemService.get(id);
        ItemForm itemForm = new ItemForm();

        itemForm.setAnimalInfo(item.getAnimalInfo());
        itemForm.setToyInfo(item.getToyInfo());

        model.addAttribute("item", item);
        model.addAttribute("itemForm", itemForm);
        return "admin/editItem";
    }

    @RequestMapping(value = "/item/{id}/edit", method = RequestMethod.POST)
    public String EditItemPost(@PathVariable("id") int id,@RequestParam MultipartFile file, ModelMap model, ItemForm itemForm, BindingResult result) throws Exception {
//		passwordValidator.setUser(sentUser);
        itemValidator.validate(itemForm, result);
        if (result.hasErrors()) {
            return "admin/editItem";
        }
        Item item = itemService.get(id);
        item.setToyTitle(itemForm.getToyTitle());
        item.setToyInfo(itemForm.getToyInfo());
        item.setAnimalInfo(itemForm.getAnimalInfo());
        if (!file.isEmpty()) {
            itemService.update(item, file);
        } else {
            itemService.update(item);
        }
        model.addAttribute("item", item);
        return "item/item";
    }

    @RequestMapping(value = "item/{id}/delete", method = RequestMethod.DELETE)
    public void deleteItem(@PathVariable("id") int item,  ModelMap model) throws Exception {
        new ItemService().delete(item);
        model.addAttribute("item", itemService.getAll());
    }

    @RequestMapping(value = "/purchases", method = RequestMethod.GET)
    public String getAllPurchases(Model model) {
        model.addAttribute("purchases", purchaseHistoryService.getAll());
        return "admin/purchases";
    }

    @RequestMapping(value = "/purchases/item/{id}", method = RequestMethod.GET)
    public String getPurchasesByItem(Model model, @PathVariable("id") int id) {
        model.addAttribute("purchases", purchaseHistoryService.getAllPurchasesByItem(itemService.get(id)));
        return "admin/purchases";
    }

    @RequestMapping(value = "/post/{id}/edit", method = RequestMethod.GET)
    public String EditPost(ModelMap model, @PathVariable("id") int id) {
        PostForm postForm = new PostForm();
        Post post = postService.get(id);
        postForm.setInfo(post.getInfo());

        model.put("post", post);
        model.put("postForm", postForm);
        return "admin/editPost";
    }

    @RequestMapping(value = "/post/{id}/edit", method = RequestMethod.POST)
    public String processEditPost(PostForm postForm, @RequestParam MultipartFile file, BindingResult result, @PathVariable("id") int id) throws Exception {
        postValidator.validate(postForm, result);
        if (result.hasErrors()) {
            return "admin/editPost";
        }
        Post post = postService.get(id);
        post.setTitle(postForm.getTitle());
        post.setInfo(postForm.getInfo());
        if (!file.isEmpty()) {
            postService.update(post, file);
        } else {
            postService.update(post);
        }
        return "admin/admin";
    }

    @RequestMapping(value = "post/{post}/delete", method = RequestMethod.DELETE)
    public void deletePost(@PathVariable("post") int post,  ModelMap model) throws Exception {
        new PostService().delete(post);
        model.addAttribute("posts", postService.getAll());
    }

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public String getAllPosts(Model model) {
        model.addAttribute("posts", postService.getAll());
        return "admin/posts";
    }
}
