package com.wwf.controllers;

import com.wwf.entity.User;
import com.wwf.form.ChangePasswordForm;
import com.wwf.form.EditProfileForm;
import com.wwf.service.PurchaseHistoryService;
import com.wwf.service.UserService;
import com.wwf.validator.EditProfileValidator;
import com.wwf.validator.PasswordValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.security.Principal;

/**
 * Created by Владелец on 07.05.2016.
 */
@Controller
@RequestMapping("/profile")
public class ProfileController {
    UserService userService = new UserService();
    PurchaseHistoryService purchaseHistoryService = new PurchaseHistoryService();

    @Autowired
    EditProfileValidator editProfileValidator = new EditProfileValidator();

    @Autowired
    PasswordValidator passwordValidator = new PasswordValidator();

    @RequestMapping(value = "/my-profile", method = RequestMethod.GET)
    public String myProfile(ModelMap model, Principal principal) {
        User sentUser = userService.getByUsername(principal.getName());
        sentUser.setPassword("hidden");
        model.put("user", sentUser);
        sentUser.getImage();
        return "user/myProfile";
    }

    @RequestMapping(value = "/my-profile/edit", method = RequestMethod.GET)
    public String myProfileEditGet(ModelMap model, Principal principal) {
        EditProfileForm editProfileForm = new EditProfileForm();
        User sentUser = userService.getByUsername(principal.getName());
        sentUser.setPassword("hidden");
        model.put("user", sentUser);
        model.put("editProfileForm", editProfileForm);
        return "user/editProfile";
    }

    @RequestMapping(value = "/my-profile/edit", method = RequestMethod.POST)
    public String myProfileEditPost(ModelMap model, @RequestParam MultipartFile file, Principal principal, EditProfileForm editProfileForm, BindingResult result) throws Exception {
        User sentUser = userService.getByUsername(principal.getName());
        model.put("user", sentUser);
        sentUser.setPassword("hidden");
        editProfileValidator.validate(editProfileForm, result);
        if (result.hasErrors()) {
            return "user/editProfile";
        }
        sentUser.setLastName(editProfileForm.getLastName());
        sentUser.setFirstName(editProfileForm.getFirstName());
        userService.update(sentUser,file);
        return "user/myProfile";
    }

    @RequestMapping(value = "/my-profile/change-password", method = RequestMethod.GET)
    public String myProfileChangePasswordGet(ModelMap model, Principal principal) {
        ChangePasswordForm changePasswordForm = new ChangePasswordForm();
        model.put("changePasswordForm", changePasswordForm);
        return "user/changePassword";
    }

    @RequestMapping(value = "/my-profile/change-password", method = RequestMethod.POST)
    public String myProfileChangePasswordPost(ModelMap model, Principal principal, ChangePasswordForm changePasswordForm, BindingResult result) throws Exception {
        User sentUser = userService.getByUsername(principal.getName());
        passwordValidator.setUser(sentUser);
        passwordValidator.validate(changePasswordForm, result);
        if (result.hasErrors()) {
            return "user/changePassword";
        }
        sentUser.setPassword(changePasswordForm.getNewPassword());
        userService.update(sentUser);
        sentUser.setPassword("hidden");
        model.put("user", sentUser);
        return "user/myProfile";
    }

    @RequestMapping(value = "/view/{username}", method = RequestMethod.GET)
    public String somebodiesProfile(@PathVariable("username") String username, ModelMap model) {
        User sentUser = userService.getByUsername(username);
        model.put("user", sentUser);
        return "user/somebodiesProfile";
    }

    @RequestMapping(value = "/my-purchases", method = RequestMethod.GET)
    public String getAllItems(Model model, Principal principal) {
        model.addAttribute("purchases", purchaseHistoryService.getAllPurchasesByUser(userService.getByUsername(principal.getName())));
        return "user/myPurchases";
    }
}
