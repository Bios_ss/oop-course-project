package com.wwf.controllers;

import com.wwf.entity.facePart.PurchaseHistory;
import com.wwf.form.BuyingForm;
import com.wwf.service.ItemService;
import com.wwf.service.PurchaseHistoryService;
import com.wwf.service.UserService;
import com.wwf.validator.PurchaseHistoryValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

/**
 * Created by Владелец on 30.04.2016.
 */
@Controller
@RequestMapping("/")
public class ItemController {
    @Autowired
    PurchaseHistoryValidator purchaseHistoryValidator = new PurchaseHistoryValidator();

    ItemService itemService = new ItemService();
    PurchaseHistoryService purchaseHistoryService = new PurchaseHistoryService();
    UserService userService = new UserService();

    @RequestMapping(value = "/item/{item}", method = RequestMethod.GET)
    public String showItem(@PathVariable("item") int item, ModelMap model) {
        model.put("item", itemService.get(item));
        return "item/item";
    }

    @RequestMapping(value = "/buy/{item}", method = RequestMethod.GET)
    public String processBuyItemGet(@PathVariable("item") int item,  ModelMap model) throws Exception {
        BuyingForm buyingForm = new BuyingForm();
        model.put("buyingForm", buyingForm);
        model.put("item", itemService.get(item));
        System.out.println(itemService.get(item));
        System.out.println(buyingForm.getPrice());
        return "item/buy";
    }

    @RequestMapping(value = "/buy/{item}", method = RequestMethod.POST)
    public String processBuyItemPost(@PathVariable("item") int item,Principal principal, BuyingForm buyingForm, ModelMap model, BindingResult result)  {
        System.out.println(buyingForm.toString());
        model.put("item", itemService.get(item));
        model.put("price", buyingForm.getPrice());
        model.put("user", principal.getName());
        model.put("fullName", buyingForm.getFullName());
        model.put("address", buyingForm.getAddress());
        model.put("comment", buyingForm.getComment());
        purchaseHistoryValidator.validate(buyingForm, result);
        if (result.hasErrors()) {
            return "item/buy";
        }

//        System.out.println("===\n" + itemService.get(item) + "- item");
//        System.out.println(buyingForm.getPrice() + "- price");
//        System.out.println(new PurchaseHistory(buyingForm.getPrice(), itemService.get(item), userService.getByUsername(principal.getName())));
//        purchaseHistoryService.add(new PurchaseHistory(buyingForm.getPrice(), itemService.get(item), userService.getByUsername(principal.getName())));
        return "item/pay";
    }

@RequestMapping(value = "/buy/{item}/success", method = RequestMethod.POST)
public String BuyProcess(@PathVariable("item") int item, BuyingForm buyingForm, Principal principal, ModelMap model, BindingResult result)  {
    System.out.println("==========2 " + result);
    purchaseHistoryValidator.validate(buyingForm, result);
    if (result.hasErrors()) {
        return "item/pay";
    }

    buyingForm.setItem(item);
    model.put("item", itemService.get(item));
    model.put("price", buyingForm.getPrice());
    model.put("user", principal.getName());
    model.put("fullName", buyingForm.getFullName());
    model.put("address", buyingForm.getAddress());
    model.put("comment", buyingForm.getComment());
//    purchaseHistoryValidator.validate(buyingForm, result);


//    System.out.println("===\n" + itemService.get(item) + "- item");
//    System.out.println(buyingForm.getPrice() + "- price");
//    System.out.println(new PurchaseHistory(buyingForm.getPrice(), itemService.get(item), userService.getByUsername(principal.getName())));
        purchaseHistoryService.add(new PurchaseHistory(buyingForm.getPrice(), itemService.get(item), userService.getByUsername(principal.getName()), buyingForm.getFullName(), buyingForm.getAddress(), buyingForm.getComment()));
    return "item/success";
}
}
