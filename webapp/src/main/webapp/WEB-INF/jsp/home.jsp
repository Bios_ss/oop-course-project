<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--it needs for authorization (sec:authorize)-->
<%@ page session="false" %>
<html>
<head>
    <title>Home</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/myStyles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="row">
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="col-md-1">
                    <a class="navbar-brand" href="#">WWF</a>
                </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="<c:url value="/"/>">Home</a></li>
                    <li><a href="<c:url value="/items"/>">Charity & toys</a></li>
                    <li><a href="<c:url value="/find-and-lost/find"/>">Find & Lost</a></li>

                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <li><a href="<c:url value="/admin/"/>"><span class="glyphicon glyphicon-cog"></span> Admin</a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="isAuthenticated()">
                        <li><a href="<c:url value="/profile/my-profile"/>"><span
                                class="glyphicon glyphicon-user"></span>
                            Profile</a></li>
                        <li><a href="<c:url value="/logout"/>"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
                        </li>
                    </sec:authorize>

                    <sec:authorize access="isAnonymous()">
                        <li>
                            <a href="<c:url value="/registration"/>">
                                <span class="glyphicon glyphicon-user"></span> SignUp</a>
                        </li>
                        <li>
                            <a href="<c:url value="/signin"/>"><span class="glyphicon glyphicon-log-in"></span>
                                Login</a>
                        </li>
                    </sec:authorize>
                </ul>


            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>

<div class="row" id="three-posts-container">
    <div class="col-md-1">
        <!--.col-md-1-->
    </div>
    <div class="col-md-11">

        <div class="row">
            <c:forEach items="${getThreePosts}" var="post">
                <div class="col-md-3" id="font-posts">
                    <div class="effect eff-2-2">
                        <a href="/post/${post.id}"><img
                                src="${pageContext.request.contextPath}/resources/images/post/${post.image}"></a>
                        <div class="caption">
                            <h4><c:out value="${post.title}"/></h4>
                            <p><c:out value="${post.info}"/></p>
                            <a class="btn" href="/post/${post.id}" title="View More">View More</a>
                        </div>
                        <!--<div class="overlay"></div>-->
                    </div>

                        <%--<a href="/post/${post.id}"><img--%>
                        <%--src="${pageContext.request.contextPath}/resources/images/post/${post.image}"></a>--%>
                    <h4>
                        <c:out value="${post.title}"/>

                    </h4>
                    Date: <c:out value="${post.dateOfPost}"/><br>
                    <a href="/post/${post.id}"> Show more..</a>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <a href="/admin/post/${post.id}/edit"><span class="glyphicon glyphicon-edit"></span></a>
                    </sec:authorize>
                </div>
            </c:forEach>


        </div>
    </div>
    <%--<div class="col-md-1">--%>
        <%--<!--.col-md-1-->--%>
    <%--</div>--%>
</div>

<div class="row" id="empty-row"></div>

<div class="row" id="mainContainer">
    <div class="col-md-1">
        <!--.col-md-1-->
    </div>

    <div class="col-md-10" id="main">
        <div class="row" id="posts-container">
            <div class="col-md-9">

                <c:forEach items="${getAllPosts}" var="post">

                    <!--post-->
                    <div class="row" id="post">
                        <div class="col-md-5">
                            <a href="/post/${post.id}"> <img
                                    src="${pageContext.request.contextPath}/resources/images/post/${post.image}" alt=""></a>
                        </div>
                        <div class="col-md-7">
                            <h3>
                                <p><c:out value="${post.title}"/>
                                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                                        <button type="button" class="btn btn-link"
                                                onclick="location.href = '/admin/post/${post.id}/edit'"><span
                                                class="glyphicon glyphicon-edit"></span></button>
                                    </sec:authorize>
                                </p>
                            </h3>
                            <p>
                                Date: <c:out value="${post.dateOfPost}"/>
                                <c:set var="dateOfLastEdition" value="${post.dateOfLastEdition}"/>
                                <c:if test="${not empty dateOfLastEdition }">
                                    Date of last edition: <c:out value="${post.dateOfLastEdition}"/><br>
                                </c:if>
                            </p>
                            <p><c:out value="${post.info}"/>

                            </p>
                            <button type="button" class="btn btn-success" onclick="location.href = '/post/${post.id}'">
                                Show more..
                            </button>
                        </div>
                    </div>
                    <hr>
                    <!--post end-->
                </c:forEach>


                <br><br><br><br><br><br><br>
            </div>

            <div class="col-md-3" id="item-column">
                <!--<div class="row">-->
                <%--items--%>
                <c:forEach items="${getAllItems}" var="item">


                    <div id="item">
                        <a href="/item/${item.id}">
                            <img src="${pageContext.request.contextPath}/resources/images/item/${item.image}" alt="">
                        </a>
                        <h5><p><c:out value="${item.toyTitle}"/></p></h5>
                        <hr>
                    </div>
                </c:forEach>
                <%--items end--%>
                <!--</div>-->


            </div>
        </div>

    </div>

    <div class="col-md-1">
        <!--.col-md-1-->
    </div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>

</body>
</html>
