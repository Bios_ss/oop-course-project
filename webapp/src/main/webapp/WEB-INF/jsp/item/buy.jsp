<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--it needs for authorization (sec:authorize)-->
<%@ page session="false" %>
<html>
<head>
    <title>Buy</title>
    <style type="text/css">
        span.error {
            color: red;
        }
    </style>
    <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jQuery/jquery-1.11.0.min.js"></script>

    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/myStyles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<div class="row">
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="col-md-1">
                    <a class="navbar-brand" href="#">WWF</a>
                </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="<c:url value="/"/>">Home</a></li>
                    <li><a href="<c:url value="/items"/>">Charity & toys</a></li>
                    <li><a href="<c:url value="/find-and-lost/find"/>">Find & Lost</a></li>

                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <li><a href="<c:url value="/admin/"/>"><span class="glyphicon glyphicon-cog"></span> Admin</a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="isAuthenticated()">
                        <li><a href="<c:url value="/profile/my-profile"/>"><span
                                class="glyphicon glyphicon-user"></span>
                            Profile</a></li>
                        <li><a href="<c:url value="/logout"/>"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
                        </li>
                    </sec:authorize>

                    <sec:authorize access="isAnonymous()">
                        <li>
                            <a href="<c:url value="/registration"/>">
                                <span class="glyphicon glyphicon-user"></span> SignUp</a>
                        </li>
                        <li>
                            <a href="<c:url value="/signin"/>"><span class="glyphicon glyphicon-log-in"></span>
                                Login</a>
                        </li>
                    </sec:authorize>
                </ul>


            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>


<!--<div class="row" id="empty-row"></div>-->

<div class="row" id="mainContainer">
    <div class="col-md-3">
        <!--.col-md-1-->
    </div>

    <div class="col-md-6" id="main">
        <div class="row" id="posts-container">
            <!--1 item  -->
            <div id="line"><br></div>
            <div class="col-md-12">
                <div class="row" id="item-details">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <form:form method="post" commandName="buyingForm">
                                <table>
                                    <tr>
                                        <td>Item:</td>
                                        <td><c:out value="${item.toyTitle}"/></td>
                                    </tr>
                                    <tr>
                                        <td><br></td>
                                    <tr>
                                        <td>Full Name:</td>
                                        <td><form:input type="text" path="fullName" size="40"
                                                        placeholder="Peter Markham Scott"/>
                                            <br><span class="error"><form:errors path="fullName"/></span>
                                        </td>
                                        <td><br></td>
                                    </tr>
                                    <tr>
                                        <td><br></td><!--ALT + 255-->
                                    </tr>
                                    <tr>
                                        <td>Address:</td>
                                        <td>
                                            <form:textarea path="address" cols="41" rows="3"
                                                           placeholder="Av. du Mont-Blanc 1196 Gland, Switzerland"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>

                                        </td>
                                        <td>
                                            <span class="error"><form:errors path="address"/></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><br></td><!--ALT + 255-->
                                    </tr>
                                    <tr>
                                        <td>Comment:</td>
                                        <td><form:textarea path="comment" cols="41" rows="3"
                                                           placeholder="Write here your commentary for your purchase, if you have something to say.."/>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td>
                                            <form:radiobutton path="price" value="15" checked="true"/>15$<Br>
                                            <span class="error"><form:errors path="price"/></span>
                                            <form:radiobutton path="price" value="25"/> 25$<Br>
                                            <span class="error"><form:errors path="price"/></span>
                                            <form:radiobutton path="price" value="50"/> 50$<Br>
                                            <span class="error"><form:errors path="price"/></span>
                                            <form:radiobutton path="price" value="100"/> 100$<Br>
                                            <span class="error"><form:errors path="price"/></span>
                                            <form:radiobutton path="price" value="250"/> 250$<Br>
                                            <span class="error"><form:errors path="price"/></span>
                                            <form:radiobutton path="price" value="500"/> 500$<Br>
                                            <span class="error"><form:errors path="price"/></span>


                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="left-btn">
                                                <button type="submit" class="btn btn-info" value="Next">Back
                                                <span class="glyphicon glyphicon-chevron-left"
                                                      style="float: left"></span>
                                                </button>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="right-button">
                                            <button type="submit" class="btn btn-success " value="Next"> Next
                                                <span class="glyphicon glyphicon-chevron-right"></span>
                                            </button>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </form:form>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
                <%--<hr>--%>
                <div class="row">
                    <div id="line"><br></div>
                </div>
            </div>

        </div>

    </div>

    <div class="col-md-3">
        <!--.col-md-1-->
    </div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
