<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--it needs for authorization (sec:authorize)-->
<%@ page session="false" %>
<html>
<head>
    <title>Item</title>
    <style type="text/css">
        span.error {
            color: red;
        }
    </style>
    <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jQuery/jquery-1.11.0.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/myStyles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<%--Delete item script--%>
<script>
    $(document).ready(function () {
        var deleteLink = $("a:contains('Delete')");
        $(deleteLink).click(function (event) {
            var confirmBox = confirm("Are you sure ?");
            if (confirmBox) {
                $.ajax({
                    url: $(event.target).attr("href"),
                    type: 'DELETE',

                    success: function () {
                        confirm("+1");
                    },
                    error: function () {
                        alert(error);
                    }

                });
            } else {
                event.preventDefault();
            }
            event.preventDefault();
            setTimeout(window.location.replace("../admin/"), 1000);

        });
    });
</script>
<%--Delete item script end--%>

<div class="row">
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="col-md-1">
                    <a class="navbar-brand" href="#">WWF</a>
                </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="<c:url value="/"/>">Home</a></li>
                    <li><a href="<c:url value="/items"/>">Charity & toys</a></li>
                    <li><a href="<c:url value="/find-and-lost/find"/>">Find & Lost</a></li>

                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <li><a href="<c:url value="/admin/"/>"><span class="glyphicon glyphicon-cog"></span> Admin</a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="isAuthenticated()">
                        <li><a href="<c:url value="/profile/my-profile"/>"><span
                                class="glyphicon glyphicon-user"></span>
                            Profile</a></li>
                        <li><a href="<c:url value="/logout"/>"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
                        </li>
                    </sec:authorize>

                    <sec:authorize access="isAnonymous()">
                        <li>
                            <a href="<c:url value="/registration"/>">
                                <span class="glyphicon glyphicon-user"></span> SignUp</a>
                        </li>
                        <li>
                            <a href="<c:url value="/signin"/>"><span class="glyphicon glyphicon-log-in"></span>
                                Login</a>
                        </li>
                    </sec:authorize>
                </ul>


            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>


<!--<div class="row" id="empty-row"></div>-->

<div class="row" id="mainContainer">
    <div class="col-md-2">
        <!--.col-md-1-->
    </div>

    <div class="col-md-8" id="main">
        <div class="row" id="posts-container">
            <!--1 item  -->
            <div class="col-md-12">
                <div class="row" id="item-details">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <img src="${pageContext.request.contextPath}/resources/images/item/${item.image}" alt="">
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    <div class="col-md-12">
                        <div><p>
                            <h3>
                                <c:out value="${item.toyTitle}"/>
                            <%--edit/delete--%>
                            <sec:authorize access="hasRole('ROLE_ADMIN')">
                                <button onclick="location.href = '/admin/item/${item.id}/edit'"
                                        class="btn btn-link"><span class="glyphicon glyphicon-edit"></span>
                                </button>
                                <button onclick="location.href = '../../admin/item/${item.id}/delete'"
                                        class="btn btn-link"><span class="glyphicon glyphicon-remove"></span>
                                </button>
                                <button onclick="location.href = '/admin/purchases/item/${item.id}'" id="right-button"
                                        class="btn btn-success">Show sales
                                </button>
                            </sec:authorize>
                            <%--edit/delete end--%>

                        </h3>
                            </p>
                            <%--/buy--%>
                            <button type="button" onclick="location.href = '/buy/${item.id}'" id="right-button"
                                    class="btn btn-primary">Buy
                            </button>
                            <%--/buy end--%>

                        </div>
                        <p>
                            <c:out value="${item.dateOfPost}"/>
                        </p>
                        <p>
                            <c:out value="${item.animalInfo}"/>
                        </p>
                        <button type="button" onclick="location.href = '/buy/${item.id}'" class="btn btn-primary">Buy
                        </button>
                    </div>
                </div>
                <hr>
            </div>
            <!--1 item end-->


        </div>

    </div>

    <div class="col-md-2">
        <!--.col-md-1-->
    </div>
</div>
<hr>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>
