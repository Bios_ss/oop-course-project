<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--it needs for authorization (sec:authorize)-->
<%@ page session="false" %>
<html>
<head>
    <title>Home</title>
</head>
<body>
<h1>Spring Security - Hello world!</h1>

<spring:url value="/secure/" var="secureUrl"/>
<a href="${secureUrl}" title="Secure">Secure</a><br/>

<spring:url value="/admin/" var="adminUrl"/>
<a href="${adminUrl}" title="Admin">Admin</a><br>

<sec:authorize access="hasRole('ROLE_ANONYMOUS')">
    <a href="/registration" title="registration">Registration</a><br>
</sec:authorize><br>

<br>
<sec:authorize access="isAuthenticated()">
    <a href="<c:url value="/logout"/>">Logout</a>
</sec:authorize>

<sec:authorize access="isAuthenticated()">
    <p>Ваш логин: <sec:authentication property="principal.username"/></p>
    <p><a class="btn btn-lg btn-danger" href="<c:url value="/logout" />" role="button">Выйти</a></p>
</sec:authorize>
<br>

<a href="/find-and-lost">Find and lost</a><br>
<a href="/profile/my-profile">My profile</a><br>
<br>

<c:forEach items="${getThreePosts}" var="post">
    Title: <c:out value="${post.title}"/><br>
    Date: <c:out value="${post.dateOfPost}"/><br>
        <a href="/post/${post.id}"> Show more..</a>
    <br>//////////////
    <hr>
</c:forEach>

<%--${getAllItems}--%>
<c:forEach items="${getAllPosts}" var="post">
    Title: <c:out value="${post.title}"/><br>
    Info: <c:out value="${post.info}"/><br>
    Date: <c:out value="${post.dateOfPost}"/><br>
    <c:set var="dateOfLastEdition" value="${post.dateOfLastEdition}"/>
    <c:if test="${not empty dateOfLastEdition }">
        Date of last edition: <c:out value="${post.dateOfLastEdition}"/><br>
    </c:if>
    <a href="/post/${post.id}"> Show more..</a>
    <br>
    <hr>
</c:forEach>
<hr>
<c:forEach items="${getAllItems}" var="item">
    id: <c:out value="${item.id}"/><br>
    Title: <c:out value="${item.toyTitle}"/><br>
    Info: <c:out value="${item.animalInfo}"/><br>
    Date: <c:out value="${item.dateOfPost}"/><br>

    <a href="/item/${item.id}"> Show more..</a>
    <br>
    <hr>
</c:forEach>

</body>
</html>
