<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--it needs for authorization (sec:authorize)-->
<%@ page session="false" %>
<html>
<head>
    <title>Posts</title>
    <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jQuery/jquery-1.11.0.min.js"></script>

    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/myStyles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<%--Delete post script--%>
<script>
    $(document).ready(function () {
        var deleteLink = $("a:contains('Delete Post')");
        $(deleteLink).click(function (event) {
            var confirmBox = confirm("Are you sure ?");
            if (confirmBox) {
                $.ajax({
                    url: $(event.target).attr("href"),
                    type: 'DELETE',

                    success: function () {
                        confirm("+1");
                    },
                    error: function () {
                        alert(error);
                    }

                });
            } else {
                event.preventDefault();
            }
            event.preventDefault();
            setTimeout(window.location.replace("../admin/"), 1000);

        });
    });
</script>
<%--Delete post script end--%>


<div class="row">
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="col-md-1">
                    <a class="navbar-brand" href="#">WWF</a>
                </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="<c:url value="/"/>">Home</a></li>
                    <li><a href="<c:url value="/items"/>">Charity & toys</a></li>
                    <li><a href="<c:url value="/find-and-lost/find"/>">Find & Lost</a></li>

                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <li><a href="<c:url value="/admin/"/>"><span class="glyphicon glyphicon-cog"></span> Admin</a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="isAuthenticated()">
                        <li><a href="<c:url value="/profile/my-profile"/>"><span
                                class="glyphicon glyphicon-user"></span>
                            Profile</a></li>
                        <li><a href="<c:url value="/logout"/>"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
                        </li>
                    </sec:authorize>

                    <sec:authorize access="isAnonymous()">
                        <li>
                            <a href="<c:url value="/registration"/>">
                                <span class="glyphicon glyphicon-user"></span> SignUp</a>
                        </li>
                        <li>
                            <a href="<c:url value="/signin"/>"><span class="glyphicon glyphicon-log-in"></span>
                                Login</a>
                        </li>
                    </sec:authorize>
                </ul>


            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>


<!--<div class="row" id="empty-row"></div>-->

<div class="row" id="mainContainer">
    <div class="col-md-2">
        <!--.col-md-1-->
    </div>

    <div class="col-md-8" id="main">
        <div class="row" id="posts-container">
            <!--1 item  -->
            <div class="col-md-12">
                <div class="row" id="line"><br></div> <!-- ALT + 255 -->
                <div class="row" id="admin-items">
                    <!--<hr>-->
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <button type="button" class="btn btn-default btn-lg btn-block"
                                    onclick="location.href = '/admin/'">Go back
                            </button>
                            <br>
                            <table class="table table-striped">
                                <tr>
                                    <td>id</td>
                                    <td>Title</td>
                                    <td>Date</td>
                                    <td>Open this page</td>
                                    <td>Edit</td>
                                    <td>Delete</td>
                                </tr>

                                <c:forEach items="${posts}" var="post">
                                    <tr>
                                        <td><c:out value="${post.id}"/></td>
                                        <td style="max-width: 230px; min-width: 50px">
                                            <c:out value="${post.title}"/>
                                        </td>
                                        <td style="max-width: 200px;">
                                            <c:out value="${post.dateOfPost}"/>
                                        </td>

                                        <td><a href="/post/${post.id}"> Show more..</a></td>

                                            <%--<td><button onclick="location.href = '/profile/view/${user.username}'" id="Show" class="float-left submit-button" >Show</button></td>--%>
                                        <td>
                                            <button onclick="location.href = '/admin/post/${post.id}/edit'" id="showSales" class="btn btn-warning">Edit</button>
                                        </td>
                                        <td>
                                            <a href="../../admin/post/${post.id}/delete">
                                                Delete Post
                                            </a>
                                    </tr>
                                </c:forEach>
                            </table>
                            <button type="button" class="btn btn-default btn-lg btn-block"
                                    onclick="location.href = '/admin/'">Go back
                            </button>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                <!--<hr>--><br>
                <div class="row" id="line"><br></div> <!-- ALT + 255 -->

            </div>

            <!--1 item end-->


        </div>

    </div>

    <div class="col-md-2">
        <!--.col-md-1-->
    </div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>

</html>
