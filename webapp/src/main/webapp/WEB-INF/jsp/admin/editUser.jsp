<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<!--it needs for authorization (sec:authorize)-->
<%@ page session="false"%>
<html>
<head>
    <title>Edit User</title>
    <style type="text/css">
        span.error {
            color: red;
        }
    </style>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/myStyles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="row">
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="col-md-1">
                    <a class="navbar-brand" href="#">WWF</a>
                </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="<c:url value="/"/>">Home</a></li>
                    <li><a href="<c:url value="/items"/>">Charity & toys</a></li>
                    <li><a href="<c:url value="/find-and-lost/find"/>">Find & Lost</a></li>

                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <li><a href="<c:url value="/admin/"/>"><span class="glyphicon glyphicon-cog"></span> Admin</a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="isAuthenticated()">
                        <li><a href="<c:url value="/profile/my-profile"/>"><span
                                class="glyphicon glyphicon-user"></span>
                            Profile</a></li>
                        <li><a href="<c:url value="/logout"/>"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
                        </li>
                    </sec:authorize>

                    <sec:authorize access="isAnonymous()">
                        <li>
                            <a href="<c:url value="/registration"/>">
                                <span class="glyphicon glyphicon-user"></span> SignUp</a>
                        </li>
                        <li>
                            <a href="<c:url value="/signin"/>"><span class="glyphicon glyphicon-log-in"></span>
                                Login</a>
                        </li>
                    </sec:authorize>
                </ul>


            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>


<!--<div class="row" id="empty-row"></div>-->

<div class="row" id="mainContainer">
    <div class="col-md-4">
        <!--.col-md-1-->
    </div>

    <div class="col-md-4" id="main">
        <div class="row" id="posts-container">
            <!--1 item  -->
            <div class="col-md-12">
                <div class="row" id="line"><br></div> <!-- ALT + 255 -->
                <div class="row" id="change-password">
                    <!--<hr>-->
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">

                            <h3>Edit profile</h3>
                            <form:form method="post" commandName="editUserForm" enctype="multipart/form-data">
                                <table>
                                    <tr>
                                        <td><br></td>
                                    </tr>

                                    <img src="${pageContext.request.contextPath}/resources/images/avatar/${user.image}"
                                         alt="">
                                    <br>

                                    <tr>
                                        <td>Username:</td>
                                        <td><c:out value="${user.username}"/></td>
                                    </tr>
                                    <tr>
                                        <td><br></td>
                                    </tr>


                                    <tr>
                                        <td>First Name:</td>
                                        <td>
                                            <form:input path="firstName" value="${user.firstName}"
                                                        class="form-control"/>
                                            <span class="error"><form:errors path="firstName"/></span>
                                        </td>
                                        <td><br></td>
                                    </tr>
                                    <tr>
                                        <td><br></td><!--ALT + 255-->
                                    </tr>
                                    <tr>
                                        <td>Last Name:</td>
                                        <td>
                                            <form:input path="lastName" value="${user.lastName}" class="form-control"/>
                                            <span class="error"><form:errors path="lastName"/></span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td><br></td><!--ALT + 255-->
                                    </tr>
                                    <tr>
                                        <td>password:</td>
                                        <td><form:input path="password" value="${user.password}" class="form-control"/></td>
                                        <td><span class="error"><form:errors path="password" /></span></td>
                                    </tr>

                                    <tr>
                                        <td><br></td><!--ALT + 255-->
                                    </tr>

                                    <tr>
                                        <td>Email:</td>
                                        <td><form:input path="email" value="${user.email}" class="form-control"/></td>
                                        <td><span class="error"><form:errors path="email" /></span></td>
                                    </tr>

                                    <tr>
                                        <td><br></td><!--ALT + 255-->
                                    </tr>


                                </table>
                                <div id="change-password-file">
                                    Change photo: <input type="file" name="file" class="btn-default"/>
                                </div>
                                <br>
                                <td colspan="3"><input type="submit" class="btn btn-success btn-lg btn-block"
                                                       value="Save changes"/></td>
                            </form:form>

                            <button type="button" class="btn btn-primary btn-sm btn-block"
                                    onclick="location.href = '/admin/users'">Go to all users
                            </button>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                </div>
                <!--<hr>--><br>
                <div class="row" id="line"><br></div> <!-- ALT + 255 -->

            </div>
        </div>
    </div>

    <div class="col-md-3">
        <!--.col-md-1-->
    </div>
</div>




<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>


</body>
</html>