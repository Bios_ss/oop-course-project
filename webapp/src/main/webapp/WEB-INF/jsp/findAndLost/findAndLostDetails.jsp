<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--it needs for authorization (sec:authorize)-->
<%@ page session="false" %>
<html>
<head>
    <title>Advert Details</title>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/myStyles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="row">
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="col-md-1">
                    <a class="navbar-brand" href="#">WWF</a>
                </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="<c:url value="/"/>">Home</a></li>
                    <li><a href="<c:url value="/items"/>">Charity & toys</a></li>
                    <li><a href="<c:url value="/find-and-lost/find"/>">Find & Lost</a></li>

                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <li><a href="<c:url value="/admin/"/>"><span class="glyphicon glyphicon-cog"></span> Admin</a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="isAuthenticated()">
                        <li><a href="<c:url value="/profile/my-profile"/>"><span
                                class="glyphicon glyphicon-user"></span>
                            Profile</a></li>
                        <li><a href="<c:url value="/logout"/>"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
                        </li>
                    </sec:authorize>

                    <sec:authorize access="isAnonymous()">
                        <li>
                            <a href="<c:url value="/registration"/>">
                                <span class="glyphicon glyphicon-user"></span> SignUp</a>
                        </li>
                        <li>
                            <a href="<c:url value="/signin"/>"><span class="glyphicon glyphicon-log-in"></span>
                                Login</a>
                        </li>
                    </sec:authorize>
                </ul>


            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>


<!--<div class="row" id="empty-row"></div>-->

<div class="row" id="mainContainer">
    <div class="col-md-2">
        <!--.col-md-1-->
    </div>

    <div class="col-md-8" id="main">
        <div class="row" id="posts-container">
            <!--1 item  -->
            <div id="line"><br></div>
            <div class="col-md-12">

                <div class="row" id="find-and-lost-details">
                    <div class="row">
                        <!--<div class="col-md-1"></div>-->
                        <div class="col-md-12">

                            <img src="${pageContext.request.contextPath}/resources/images/findAndLost/${advert.image}"
                                 alt="">

                            <div id="item-title">
                                <h3>
                                    <p>
                                        <c:set var="status" value="${advert.status}"/>
                                        <c:if test="${status == false}">
                                            <span class="glyphicon glyphicon-lock"></span>
                                        </c:if>
                                        <c:set var="findOrLost" value="${advert.findOrLost}"/>
                                        <c:if test="${findOrLost == true}">
                                            [Lost]
                                        </c:if>
                                        <c:if test="${findOrLost == false}">
                                            [Found]
                                        </c:if>
                                        <c:out value="${advert.title}"/>
                                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                                        <button onclick="location.href = '/find-and-lost/details/${advert.id}/edit'"
                                                class="btn btn-link"><span class="glyphicon glyphicon-edit"></span>
                                        </button>

                                            <button type="button" class="btn btn-link"
                                                    onclick="location.href = '/find-and-lost/details/${advert.id}/close-advert'">
                                                <span class="glyphicon glyphicon-eye-close"></span></button>
                                        </sec:authorize>
                                    </p>
                                </h3>
                                <br>Date: <c:out value="${advert.date}"/><br>
                            </div>
                            <p>Contacts:<br><c:out value="${advert.contacts}"/><br></p>
                            <p><c:out value="${advert.animalInfo}"/>
                            </p>
                        </div>
                        <!--<div class="col-md-1"></div>-->
                    </div>
                </div>
                <div id="left-btn">
                    <button type="submit" class="btn btn-info" onclick="location.href='/find-and-lost/find'">Back
                                                <span class="glyphicon glyphicon-chevron-left"
                                                      style="float: left"></span>
                    </button>
                    <br>
                    <br>
                </div>
            <%--<hr>--%>
            </div>
            <!--1 item end-->
        </div>
        <div class="row">
            <div id="line"><br></div>
        </div>

    </div>

    <div class="col-md-2">
        <!--.col-md-1-->
    </div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>