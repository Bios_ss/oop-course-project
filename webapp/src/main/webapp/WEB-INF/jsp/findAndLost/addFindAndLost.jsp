<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--it needs for authorization (sec:authorize)-->
<%@ page session="false" %>
<html>
<head>
    <title>Add Advert</title>
    <style type="text/css">
        span.error {
            color: red;
        }
    </style>
    <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jQuery/jquery-1.11.0.min.js"></script>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/myStyles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="row">
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="col-md-1">
                    <a class="navbar-brand" href="#">WWF</a>
                </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="<c:url value="/"/>">Home</a></li>
                    <li><a href="<c:url value="/items"/>">Charity & toys</a></li>
                    <li><a href="<c:url value="/find-and-lost/find"/>">Find & Lost</a></li>

                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <li><a href="<c:url value="/admin/"/>"><span class="glyphicon glyphicon-cog"></span> Admin</a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="isAuthenticated()">
                        <li><a href="<c:url value="/profile/my-profile"/>"><span
                                class="glyphicon glyphicon-user"></span>
                            Profile</a></li>
                        <li><a href="<c:url value="/logout"/>"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
                        </li>
                    </sec:authorize>

                    <sec:authorize access="isAnonymous()">
                        <li>
                            <a href="<c:url value="/registration"/>">
                                <span class="glyphicon glyphicon-user"></span> SignUp</a>
                        </li>
                        <li>
                            <a href="<c:url value="/signin"/>"><span class="glyphicon glyphicon-log-in"></span>
                                Login</a>
                        </li>
                    </sec:authorize>
                </ul>


            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>


<!--<div class="row" id="empty-row"></div>-->

<div class="row" id="mainContainer">
    <div class="col-md-3">
        <!--.col-md-1-->
    </div>

    <div class="col-md-6" id="main">
        <div class="row" id="posts-container">
            <!--1 item  -->
            <div class="col-md-12">
                <div class="row" id="line"><br></div>
                <div class="row" id="item-details">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">


                            <br>
                            <form:form method="post" commandName="findAndLostForm" enctype="multipart/form-data">
                                <table>
                                    <tr>
                                        <td>Title:</td>
                                        <td><form:input path="title" size="40"
                                                        placeholder="Advertisment title"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <span class="error"><form:errors path="title"/></span></td>
                                    </tr>
                                    <tr>
                                        <td><br></td><!--ALT + 255-->
                                    </tr>
                                    <tr>
                                        <td>Animal info:</td>
                                        <td>
                                            <form:textarea path="animalInfo" cols="41" rows="3"
                                                           placeholder="Describe here your animal"/>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td colspan="2">
                                            <span class="error"><form:errors path="animalInfo"/></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><br></td><!--ALT + 255-->
                                    </tr>
                                    <tr>
                                        <td>Contacts:</td>
                                        <td><form:textarea path="contacts" cols="41" rows="3"
                                                           placeholder="Write your contacts"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><span class="error"><form:errors path="contacts"/></span></td>
                                    </tr>
                                    <tr>
                                        <td><form:radiobutton path="findOrLost" value="0" checked="true"/>Find<Br></td>
                                        <td><span class="error"><form:errors path="findOrLost"/></span></td>
                                    </tr>
                                    <tr>
                                        <td><form:radiobutton path="findOrLost" value="1"/>Lost<Br></td>
                                        <td><span class="error"><form:errors path="findOrLost"/></span></td>
                                    </tr>
                                </table>
                                <br>
                                <div id="change-password-file">
                                    Add photo: <input type="file" name="file" class="btn-default"/>
                                </div>
                                <br>

                                <button type="submit" class="btn btn-success btn-lg btn-block">Add new advert
                                </button>

                            </form:form>
                            <button type="button" class="btn btn-default btn-lg btn-block"
                                    onclick="location.href = '/admin/'">Go to admin panel
                            </button>
                            <br>
                        </div>
                        <div class="col-md-2"></div>
                    </div>

                </div>
                <%--<hr>--%>
                <div class="row" id="line"><br></div>
            </div>
            <!--1 item end-->


        </div>

    </div>

    <div class="col-md-3">
        <!--.col-md-1-->
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
