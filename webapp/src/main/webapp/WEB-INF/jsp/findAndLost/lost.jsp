<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--it needs for authorization (sec:authorize)-->
<%@ page session="false" %>
<html>
<head>
    <title>lost</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/myStyles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>


<div class="row">
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="col-md-1">
                    <a class="navbar-brand" href="#">WWF</a>
                </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="<c:url value="/"/>">Home</a></li>
                    <li><a href="<c:url value="/items"/>">Charity & toys</a></li>
                    <li class="active"><a href="<c:url value="/find-and-lost/find"/>">Find & Lost</a></li>

                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <li><a href="<c:url value="/admin/"/>"><span class="glyphicon glyphicon-cog"></span> Admin</a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="isAuthenticated()">
                        <li><a href="<c:url value="/profile/my-profile"/>"><span
                                class="glyphicon glyphicon-user"></span>
                            Profile</a></li>
                        <li><a href="<c:url value="/logout"/>"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
                        </li>
                    </sec:authorize>

                    <sec:authorize access="isAnonymous()">
                        <li>
                            <a href="<c:url value="/registration"/>">
                                <span class="glyphicon glyphicon-user"></span> SignUp</a>
                        </li>
                        <li>
                            <a href="<c:url value="/signin"/>"><span class="glyphicon glyphicon-log-in"></span>
                                Login</a>
                        </li>
                    </sec:authorize>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
        <!--2-nd navbar-->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"><br>
            <ul class="nav nav-pills nav-justified">
                <li><a href="<c:url value="/find-and-lost/find"/>">Find</a></li>
                <li class="active"><a href="<c:url value="/find-and-lost/lost"/>">Lost</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
        <!--2-nd navbar end-->
    </nav>
</div>


<%--<div class="row" id="empty-row"></div>--%>

<div class="row" id="mainContainer">
    <div class="col-md-1">
        <!--.col-md-1-->
    </div>

    <div class="col-md-10" id="main">
        <sec:authorize access="isAuthenticated()">
            <div id="add-find-and-lost">
                <button class="btn btn-success" onclick="location.href = '/find-and-lost/add'">Add advert</button>
            </div>
        </sec:authorize>
        <div class="row" id="posts-container">
            <div class="col-md-12">
                <c:forEach items="${lost}" var="lost">
                    <!--post-->
                    <div class="row" id="find-and-lost">
                        <div class="col-md-3">
                            <a href="/find-and-lost/details/${lost.id}">
                                <img src="/resources/images/findAndLost/${lost.image}">
                            </a>
                        </div>
                        <div class="col-md-9">
                            <h3>
                                <p>
                                        <%--lock/unlock status--%>
                                    <c:set var="status" value="${lost.status}"/>
                                    <c:if test="${status == false}">
                                        <span class="glyphicon glyphicon-lock"></span>
                                    </c:if>
                                        <%--lock/unlock status end--%>

                                        <%--[Found/Lost]--%>
                                    <c:set var="findOrLost" value="${lost.findOrLost}"/>
                                    <c:if test="${findOrLost == true}">
                                        [Lost]
                                    </c:if>
                                    <c:if test="${findOrLost == false}">
                                        [Found]
                                    </c:if>
                                        <%--[Found/Lost] end--%>
                                    <c:out value="${lost.title}"/>
                                        <%--is user author--%>
                                    <c:set var="currentUserId" value="${currentUserId}"/>
                                    <c:set var="lostId" value="${lost.user.id}"/>
                                    <c:if test="${currentUserId == lostId }">
                                        <button type="button" class="btn btn-link"
                                                onclick="location.href = '/find-and-lost/details/${lost.id}/close-advert'">
                                            <span class="glyphicon glyphicon-eye-close"></span></button>
                                        <button type="button" class="btn btn-link"
                                                onclick="location.href = '/find-and-lost/details/${lost.id}/edit'"><span
                                                class="glyphicon glyphicon-edit"></span></button>
                                    </c:if>
                                        <%--is user admin--%>
                                    <c:if test="${currentUserId != lostId }">
                                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                                            <button type="button" class="btn btn-link"
                                                    onclick="location.href = '/find-and-lost/details/${lost.id}/edit'"><span
                                                    class="glyphicon glyphicon-edit"></span></button>
                                            <button type="button" class="btn btn-link"
                                                    onclick="location.href = '/find-and-lost/details/${lost.id}/close-advert'">
                                                <span class="glyphicon glyphicon-eye-close"></span></button>
                                        </sec:authorize>
                                    </c:if>

                                </p>
                            </h3>
                            <p>Author: <a href="/profile/view/${lost.user.username}"> <c:out
                                    value="${lost.user.username}"/></a>
                                <c:out value="${lost.date}"/>
                            </p>
                            <p>
                                <c:out value="${lost.animalInfo}"/>
                            </p>
                            <button type="button" class="btn btn-success"
                                    onclick="location.href = '/find-and-lost/details/${lost.id}'">Show more
                            </button>
                        </div>
                    </div>
                    <hr>
                    <!--post end-->
                </c:forEach>
            </div>
        </div>

    </div>

    <div class="col-md-1">
        <!--.col-md-1-->
    </div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
