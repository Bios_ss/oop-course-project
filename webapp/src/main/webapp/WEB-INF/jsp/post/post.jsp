<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--it needs for authorization (sec:authorize)-->
<%@ page session="false" %>
<html>
<head>
    <title>Post</title>
    <style type="text/css">
        span.error {
            color: red;
        }
    </style>
    <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jQuery/jquery-1.11.0.min.js"></script>

    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/myStyles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<%--Add comment script--%>
<script>
    $(document).ready(function () {
        $("#submit").click(function () {
            <sec:authorize access="isAuthenticated()">
            var comment = {
                text: $("#text").val(),
                post: $("#post").val()
            };

            $.ajax({
                url: "/post/${getPost.id}",
                contentType: 'application/json',
                data: JSON.stringify(comment),
                type: 'POST',
                success: function (data) {
//					location.reload();
                    <%--window.location.href  =  "/post/${getPost.id}";--%>
                },
                error: function (xhr, status, errorThrown) {
                    alert("Adding failed: " + status + " " + errorThrown);
                }
            });
        });
        </sec:authorize>
    });
</script>
<%--Add comment script end--%>

<%--Delete comment script--%>
<script>
    $(document).ready(function () {
        var deleteLink = $("a:contains('Delete Comment')");
        $(deleteLink).click(function (event) {
            var confirmBox = confirm("Are you sure ?");
            if (confirmBox) {
                $.ajax({
                    url: $(event.target).attr("href"),
                    type: 'DELETE',

                    success: function () {
                        confirm("+1");
                    },
                    error: function () {
                        alert(error);
                    }

                });
            } else {
                event.preventDefault();
            }
            event.preventDefault();
            setTimeout(window.location.replace("/post/${getPost.id}"), 1000);

        });
    });
</script>
<%--Delete comment script end--%>
<%--Delete post script--%>
<script>
    $(document).ready(function () {
        var deleteLink = $("a:contains('Delete Post')");
        $(deleteLink).click(function (event) {
            var confirmBox = confirm("Are you sure ?");
            if (confirmBox) {
                $.ajax({
                    url: $(event.target).attr("href"),
                    type: 'DELETE',

                    success: function () {
                        confirm("+1");
                    },
                    error: function () {
                        alert(error);
                    }

                });
            } else {
                event.preventDefault();
            }
            event.preventDefault();
            setTimeout(window.location.replace("../admin/"), 1000);

        });
    });
</script>
<%--Delete post script end--%>


<div class="row">
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="col-md-1">
                    <a class="navbar-brand" href="#">WWF</a>
                </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="<c:url value="/"/>">Home</a></li>
                    <li><a href="<c:url value="/items"/>">Charity & toys</a></li>
                    <li><a href="<c:url value="/find-and-lost/find"/>">Find & Lost</a></li>

                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <li><a href="<c:url value="/admin/"/>"><span class="glyphicon glyphicon-cog"></span> Admin</a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="isAuthenticated()">
                        <li><a href="<c:url value="/profile/my-profile"/>"><span
                                class="glyphicon glyphicon-user"></span>
                            Profile</a></li>
                        <li><a href="<c:url value="/logout"/>"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
                        </li>
                    </sec:authorize>

                    <sec:authorize access="isAnonymous()">
                        <li>
                            <a href="<c:url value="/registration"/>">
                                <span class="glyphicon glyphicon-user"></span> SignUp</a>
                        </li>
                        <li>
                            <a href="<c:url value="/signin"/>"><span class="glyphicon glyphicon-log-in"></span>
                                Login</a>
                        </li>
                    </sec:authorize>
                </ul>


            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>


<!--<div class="row" id="empty-row"></div>-->

<div class="row" id="mainContainer">
    <div class="col-md-2">
        <!--.col-md-1-->
    </div>

    <div class="col-md-8" id="main">
        <div class="row" id="posts-container">
            <!--1 item  -->
            <div id="line"><br></div>
            <div class="col-md-12">
                <div class="row" id="item-details">
                    <div class="row">

                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <img src="${pageContext.request.contextPath}/resources/images/post/${getPost.image}" alt="">
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    <div class="col-md-12">
                        <div id="item-title">
                            <p style="margin-left: 10px">
                            <h3><c:out value="${getPost.title}"/>
                                <sec:authorize access="hasRole('ROLE_ADMIN')">

                                    <button onclick="location.href = '/admin/post/${getPost.id}/edit'"
                                            class="btn btn-link"><span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                </sec:authorize>

                            </h3>

                            <sec:authorize access="hasRole('ROLE_ADMIN')">
                                <a href="../../admin/post/${getPost.id}/delete">
                                    <span class="glyphicon glyphicon-remove"></span>
                                    Delete Post
                                </a>
                            </sec:authorize>
                            </p>
                            <br><c:out value="${getPost.dateOfPost}"/>
                            <c:set var="dateOfLastEdition" value="${getPost.dateOfLastEdition}"/>
                            <c:if test="${not empty dateOfLastEdition }">
                                Date of last edition: <c:out value="${getPost.dateOfLastEdition}"/><br>
                            </c:if>
                        </div>
                        <p>
                            <c:out value="${getPost.info}"/>
                        </p>
                        <br><br>
                    </div>
                </div>
                <hr>
                <!--Comments-->


                <%--Output comments--%>
                <c:forEach items="${getComments}" var="comment">
                    <div>

                    </div>

                    <%--Output comments END--%>


                    <div class="row" id="comment">
                        <div class="col-md-3">
                            <img src="${pageContext.request.contextPath}/resources/images/avatar/${comment.user.image}"
                                 alt="">
                        </div>
                        <div class="col-md-9"></div>

                        Author: <a href="/profile/view/${comment.user.username}"> <c:out
                            value="${comment.user.username}"/></a> |
                        Date: <c:out value="${comment.date}"/>
                        <sec:authorize access="isAuthenticated()">

                            <sec:authentication property="principal.username"
                                                var="uname"/> <%--Make "uname" variable from principal.username --%>
                            <c:if test="${comment.user.username == uname}">
                                <a href="/post/${getPost.id}/delete/${comment.id}">
                                    <span class="glyphicon glyphicon-remove"></span>
                                    Delete Comment
                                </a>
                            </c:if>
                        </sec:authorize>
                        <hr>
                        <p>
                            <c:out value="${comment.text}"/>
                        </p>
                    </div>
                    <hr>
                </c:forEach>

                <%--Your comment--%>
                <div class="row" id="comment">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-9">
                        <sec:authorize access="isAuthenticated()">
                            <form:form method="post" commandName="addCommentForm">
                                <table>
                                    <tr>
                                        <td>Your comment:</td>
                                        <td><form:textarea path="text" id="text" cols="40" rows="5"/></td>
                                        <td><span class="error"><form:errors path="text"/></span></td>
                                    </tr>

                                    <tr>
                                        <td colspan="3"><input type="submit" class="btn btn-success" value="Submit"
                                                               id="right-button"/></td>
                                    </tr>
                                </table>
                            </form:form>
                        </sec:authorize>
                    </div>
                </div>

                <%--Your comment END--%>

                <!--Comments end-->

                <div class="row">
                    <div id="line"><br></div>
                </div>
            </div>
            <!--1 item end-->
        </div>
    </div>
    <div class="col-md-2">
        <!--.col-md-1-->
    </div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>

</body>
</html>
