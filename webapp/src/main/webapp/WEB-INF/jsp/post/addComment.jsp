<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<html>
<head>
    <title>AddItem</title>
    <style type="text/css">
        span.error {
            color: red;
        }
    </style>
</head>
<body>
<h1>Sign Up</h1>

<form:form method="post" commandName="addCommentForm">
    <table>
        <tr>
            <td>text:</td>
            <td><form:input path="text"/></td>
            <td><span class="error"><form:errors path="text" /></span></td>
        </tr>

        <tr>
            <td>user:</td>
            <td><form:input path="user" value=""/></td>
            <td><span class="error"><form:errors path="user" /></span></td>
        </tr>

        <tr>
            <td>post:</td>
            <td><form:input path="post" value=""/></td>
            <td><span class="error"><form:errors path="post" /></span></td>
        </tr>

        <tr>
            <td colspan="3"><input type="submit" value="Submit" /></td>
        </tr>
    </table>
</form:form>

<a href="${pageContext.request.contextPath}/" title="Home">Home</a>
</body>
</html>