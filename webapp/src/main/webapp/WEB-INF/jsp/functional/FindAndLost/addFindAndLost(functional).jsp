<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--it needs for authorization (sec:authorize)-->
<%@ page session="false" %>
<html>
<head>
    <title>Home</title>
    <style type="text/css">
        span.error {
            color: red;
        }
    </style>
    <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jQuery/jquery-1.11.0.min.js"></script>

</head>
<body>
<form:form method="post" commandName="findAndLostForm"  enctype="multipart/form-data">
    <table>
        <tr>
            <td>title:</td>
            <td><form:input path="title"/></td>
            <td><span class="error"><form:errors path="title"/></span></td>
        </tr>

        <tr>
            <td>animalInfo:</td>
            <td><form:input path="animalInfo" value=""/></td>
            <td><span class="error"><form:errors path="animalInfo"/></span></td>
        </tr>


        <tr>
            <td>contacts:</td>
            <td><form:input path="contacts" value=""/></td>
            <td><span class="error"><form:errors path="contacts"/></span></td>
        </tr>

        <tr>
            <td><form:radiobutton path="findOrLost" value="0" checked="true"/>Find<Br></td>
            <td><span class="error"><form:errors path="findOrLost"/></span></td>
        </tr>
        <tr>
            <td><form:radiobutton path="findOrLost" value="1"/>Lost<Br></td>
            <td><span class="error"><form:errors path="findOrLost"/></span></td>
        </tr>

        <tr>
            <input type="file" name="file"/>
        </tr>

        <tr>
            <td colspan="3"><input type="submit" value="Submit"/></td>
        </tr>
    </table>
</form:form>

</body>
</html>
