<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<html>
<head>
    <title>AddItem</title>
    <style type="text/css">
        span.error {
            color: red;
        }
    </style>
</head>
<body>
<h1>Sign Up</h1>

<form:form method="post" commandName="findAndLostForm"  enctype="multipart/form-data">
    <table>
        <tr>
            <td>title:</td>
            <td><form:input path="title" value="${advert.title}"/></td>
            <td><span class="error"><form:errors path="title"/></span></td>
        </tr>

        <tr>
            <td>animalInfo:</td>
            <td><form:input path="animalInfo" value="${advert.animalInfo}"/></td>
            <td><span class="error"><form:errors path="animalInfo"/></span></td>
        </tr>


        <tr>
            <td>contacts:</td>
            <td><form:input path="contacts" value="${advert.contacts}"/></td>
            <td><span class="error"><form:errors path="contacts"/></span></td>
        </tr>

        <tr>
            <input type="file" name="file"/>
        </tr>

        <tr>
            <td colspan="3"><input type="submit" value="Submit"/></td>
        </tr>
    </table>
</form:form>

<a href="${pageContext.request.contextPath}/" title="Home">Home</a>


</body>
</html>