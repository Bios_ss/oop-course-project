<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--it needs for authorization (sec:authorize)-->
<%@ page session="false"%>
<html>
<head>
</head>
<body>
<button onclick="location.href = '/find-and-lost'" id="goBack" class="float-left submit-button" >Go back</button><br>
<br>
<c:forEach items="${lost}" var="lost">
    Title: <c:out value="${lost.title}"/><br>
    Info: <c:out value="${lost.animalInfo}"/><br>
    Date: <c:out value="${lost.date}"/><br>
    Contacts: <c:out value="${lost.contacts}"/><br>

    <%--<c:set var="dateOfLastEdition" value="${post.dateOfLastEdition}"/>--%>
    <%--<c:if test="${not empty dateOfLastEdition }">--%>
    <%--Date of last edition: <c:out value="${post.dateOfLastEdition}"/><br>--%>
    <%--</c:if>--%>
    <a href="/find-and-lost/details/${lost.id}"> Show more..</a>
    <br>
    <hr>
</c:forEach>

</body>
</html>