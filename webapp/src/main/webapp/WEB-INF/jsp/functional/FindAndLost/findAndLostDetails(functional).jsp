<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--it needs for authorization (sec:authorize)-->
<%@ page session="false" %>
<html>
<head>
</head>
<body>
<button onclick="location.href = '/find-and-lost'" id="goBack" class="float-left submit-button">Go back</button>
<br>
<br>


Title: <c:out value="${advert.title}"/><br>
Info: <c:out value="${advert.animalInfo}"/><br>
Date: <c:out value="${advert.date}"/><br>
Contacts: <c:out value="${advert.contacts}"/><br>
Status: <c:out value="${advert.status}"/><br>
<img src="${pageContext.request.contextPath}/resources/images/findAndLost/${advert.image}" alt="">
<sec:authorize access="isAuthenticated()">
    <a href="/find-and-lost/details/${advert.id}/close-advert">Close</a>
</sec:authorize>
<button onclick="location.href = '/find-and-lost/details/${advert.id}/edit'" id="editButton" class="float-left submit-button">Edit</button>


</body>
</html>