<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--it needs for authorization (sec:authorize)-->
<%@ page session="false"%>
<html>
<head>
<title>Home</title>
	<style type="text/css">
		span.error {
			color: red;
		}
	</style>
	<script type="text/javascript" src="http://ajax.microsoft.com/ajax/jQuery/jquery-1.11.0.min.js"></script>

</head>
<body>
<%--Add comment script--%>
<script>
	$(document).ready(function(){
		$("#submit").click(function(){
			<sec:authorize access="isAuthenticated()">
			var comment = {
				text : $("#text").val(),
				post : $("#post").val()
			};

			$.ajax({
				url: "/post/${getPost.id}",
				contentType: 'application/json',
				data: JSON.stringify(comment),
				type: 'POST',
				success: function(data){
//					location.reload();
					<%--window.location.href  =  "/post/${getPost.id}";--%>
				},
				error: function(xhr, status, errorThrown){
					alert("Adding failed: " + status + " " + errorThrown);
				}
			});
		});
		</sec:authorize>
	});
</script>
<%--Add comment script end--%>

<%--Delete comment script--%>
<script>
	$(document).ready(function() {
		var deleteLink = $("a:contains('Delete Comment')");
		$(deleteLink).click(function(event) {
			var confirmBox = confirm("Are you sure ?");
			if(confirmBox){
				$.ajax({
					url: $(event.target).attr("href"),
					type: 'DELETE',

					success: function() {
						confirm("+1");
					},
					error: function(){
						alert(error);
					}

				});
			} else {
				event.preventDefault();
			}
			event.preventDefault();
			setTimeout(window.location.replace("/post/${getPost.id}"), 1000);

		});
	});
</script>
<%--Delete comment script end--%>
<%--Delete post script--%>
<script>
	$(document).ready(function() {
		var deleteLink = $("a:contains('Delete Post')");
		$(deleteLink).click(function(event) {
			var confirmBox = confirm("Are you sure ?");
			if(confirmBox){
				$.ajax({
					url: $(event.target).attr("href"),
					type: 'DELETE',

					success: function() {
						confirm("+1");
					},
					error: function(){
						alert(error);
					}

				});
			} else {
				event.preventDefault();
			}
			event.preventDefault();
			setTimeout(window.location.replace("../admin/"), 1000);

		});
	});
</script>
<%--Delete post script end--%>


	<h1>Spring Security - Hello world!</h1>


	<sec:authorize access="isAuthenticated()">
		<p>Ваш логин: <sec:authentication property="principal.username" /></p>
		<p><a class="btn btn-lg btn-danger" href="<c:url value="/logout" />" role="button">Logout</a></p>
	</sec:authorize>
	<br>

	<%--${getAllItems}--%>
		Title:    <c:out value="${getPost.title}"/><br>
		Info:     <c:out value="${getPost.info}"/><br>
		Date:	  <c:out value="${getPost.dateOfPost}"/><br>
		<c:set var="dateOfLastEdition" value="${getPost.dateOfLastEdition}"/>
		<c:if test="${not empty dateOfLastEdition }">
			Date of last edition:  <c:out value="${getPost.dateOfLastEdition}"/><br>
		</c:if>
<img src="${pageContext.request.contextPath}/resources/images/post/${getPost.image}" alt="">


<sec:authorize access="hasRole('ROLE_ADMIN')">
	<button onclick="location.href = '/admin/post/${getPost.id}/edit'" id="showSales" class="float-left submit-button">Edit</button>
	<%--<br>--%>
	<a href="../../admin/post/${getPost.id}/delete">Delete Post</a>
</sec:authorize>
<br><br>

    <%--Output comments--%>
    <c:forEach items="${getComments}" var="comment">
		<div>
			<tr>
				<td>id:		  <c:out value="${comment.id}"/><br></td>
				<td>Date:		  <c:out value="${comment.date}"/><br></td>
				<td>Author:		  <c:out value="${comment.user.username}"/><br></td>
				<td>Comment:        <c:out value="${comment.text}"/><br></td>
				<sec:authorize access="isAuthenticated()">

					<sec:authentication property="principal.username" var="uname" /> <%--Make "uname" variable from principal.username --%>
					<c:if test="${comment.user.username == uname}">
						<td><a href="/post/${getPost.id}/delete/${comment.id}">Delete Comment</a></td>
					</c:if>

				</sec:authorize>
   		    <%--<c:out value="${comment.user.password}"/><br>--%>
			<br>
			</tr>
		</div>
		<hr>
	</c:forEach>
    <%--Output comments END--%>


    <%--Your comment--%>
    <sec:authorize access="isAuthenticated()">
		<form:form method="post" commandName="addCommentForm">
			<table>
				<tr>
					<td>Your comment:</td>
					<td><form:input path="text" id="text"/></td>
					<td><span class="error"><form:errors path="text" /></span></td>
				</tr>

				<tr>
					<td colspan="3"><input type="submit" value="Submit" /></td>
				</tr>
			</table>
		</form:form>
    </sec:authorize>
    <%--Your comment END--%>

    <br>
		<hr>


</body>
</html>
