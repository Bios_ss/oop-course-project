<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--it needs for authorization (sec:authorize)-->
<%@ page session="false" %>
<html>
<head>
    <title>Home</title>
    <style type="text/css">
        span.error {
            color: red;
        }
    </style>
    <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jQuery/jquery-1.11.0.min.js"></script>

</head>
<body>
<%--Delete item script--%>
<script>
    $(document).ready(function () {
        var deleteLink = $("a:contains('Delete')");
        $(deleteLink).click(function (event) {
            var confirmBox = confirm("Are you sure ?");
            if (confirmBox) {
                $.ajax({
                    url: $(event.target).attr("href"),
                    type: 'DELETE',

                    success: function () {
                        confirm("+1");
                    },
                    error: function () {
                        alert(error);
                    }

                });
            } else {
                event.preventDefault();
            }
            event.preventDefault();
            setTimeout(window.location.replace("../admin/"), 1000);

        });
    });
</script>
<%--Delete item script end--%>

<h1>Spring Security - Hello world!</h1>


<sec:authorize access="isAuthenticated()">
    <p>Your username: <sec:authentication property="principal.username"/></p>
    <p><a class="btn btn-lg btn-danger" href="<c:url value="/logout" />" role="button">Logout</a></p>
</sec:authorize>
<br>

<sec:authorize access="hasRole('ROLE_ADMIN')">
    <button onclick="location.href = '/admin/purchases/item/${item.id}'" id="showSales"
            class="float-left submit-button">Show sales
    </button>
    <button onclick="location.href = '/admin/item/${item.id}/edit'" id="edit" class="float-left submit-button">Edit
    </button>
    <a href="../../admin/item/${item.id}/delete">Delete</a>
    <br>
</sec:authorize>

id: <c:out value="${item.id}"/><br>
Title: <c:out value="${item.toyTitle}"/><br>
Info: <c:out value="${item.animalInfo}"/><br>
Date: <c:out value="${item.dateOfPost}"/><br>
<img src="${pageContext.request.contextPath}/resources/images/item/${item.image}" alt="">

<br>
<hr>

<button onclick="location.href = '/buy/${item.id}'" id="buyButton" class="float-left submit-button">Buy</button>

</body>
</html>
