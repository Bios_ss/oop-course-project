<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--it needs for authorization (sec:authorize)-->
<%@ page session="false"%>
<html>
<head>
<title>Home</title>
	<style type="text/css">
		span.error {
			color: red;
		}
	</style>
	<script type="text/javascript" src="http://ajax.microsoft.com/ajax/jQuery/jquery-1.11.0.min.js"></script>

</head>
<body>
<%--Add comment script--%>

	<h1>Spring Security - Hello world!</h1>


	<sec:authorize access="isAuthenticated()">
		<p>Your username: <sec:authentication property="principal.username" /></p>
		<p><a class="btn btn-lg btn-danger" href="<c:url value="/logout" />" role="button">Logout</a></p>
	</sec:authorize>
	<br>

	<%--${getAllItems}--%>
		p:	      <c:out value="${item.id}"/><br>
		Title:    <c:out value="${item.toyTitle}"/><br>
		Price:    <c:out value="${price}"/><br>
		User:     <c:out value="${user}"/><br>
		Address:     <c:out value="${address}"/><br>
		Full Name:<c:out value="${fullName}"/><br>
		comment:  <c:out value="${comment}"/>
		<c:set var="comment" value="${comment}"/>
		<c:if test="${empty comment }">
			Comment is empty
		</c:if><br>

<br>
<hr>
	<form:form method="post" commandName="buyingForm" action="/buy/${item.id}/success">
		<form:input path="price" value="${price}" cssStyle="visibility: hidden"/>
		<form:input path="fullName" value="${fullName}" cssStyle="visibility: hidden"/>
		<form:input path="address" value="${address}" cssStyle="visibility: hidden"/>
		<form:input path="comment" value="${comment}" cssStyle="visibility: hidden"/>
		<input type="submit" value="Pay">
	</form:form>

<%--<button onclick="location.href = '/buy/${item.id}/success'" id="buyButton" class="float-left submit-button" >Pay</button>--%>


</body>
</html>
