<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<html>
<head>
    <title>AddItem</title>
    <style type="text/css">
        span.error {
            color: red;
        }
    </style>
</head>
<body>
<h1>Change password</h1>

<form:form method="post" commandName="changePasswordForm">
    <table>
        <tr>
            <td>currentPassword:</td>
            <td><form:input path="currentPassword" /></td>
            <td><span class="error"><form:errors path="currentPassword" /></span></td>
        </tr>

        <tr>
            <td>newPassword:</td>
            <td><form:input path="newPassword" /></td>
            <td><span class="error"><form:errors path="newPassword" /></span></td>
        </tr>
        <tr>
            <td>confirmNewPassword:</td>
            <td><form:input path="confirmNewPassword" /></td>
            <td><span class="error"><form:errors path="confirmNewPassword" /></span></td>
        </tr>

        <tr>
            <td colspan="3"><input type="submit" value="Submit" /></td>
        </tr>
    </table>
</form:form>

</body>
</html>