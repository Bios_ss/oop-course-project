<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--it needs for authorization (sec:authorize)-->
<%@ page session="false"%>
<html>
<head>
    <title>Home</title>
    <style type="text/css">
        span.error {
            color: red;
        }
    </style>
    <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jQuery/jquery-1.11.0.min.js"></script>

</head>
<body>
<%--Add comment script--%>

<h1>Spring Security - Hello world!</h1>


<sec:authorize access="isAuthenticated()">
    <p>Your username: <sec:authentication property="principal.username" /></p>
    <p><a class="btn btn-lg btn-danger" href="<c:url value="/logout" />" role="button">Logout</a></p>
</sec:authorize>
<br>
<hr>

<%--${getAllItems}--%>
username:     <c:out value="${user.username}"/><br>
<sec:authorize access="hasRole('ROLE_ADMIN')">
    Password:    <c:out value="${user.password}"/><br>
    Role:    <c:out value="${user.role}"/><br>
</sec:authorize>
firstName:    <c:out value="${user.firstName}"/><br>
lastName:	  <c:out value="${user.lastName}"/><br>
email:	      <c:out value="${user.email}"/><br>
<br>
<sec:authorize access="hasRole('ROLE_ADMIN')">
    <button onclick="location.href = '/admin/user/${user.username}/edit'" id="Edit" class="float-left submit-button" >Edit</button><br>
</sec:authorize>
<img src="${pageContext.request.contextPath}/resources/images/avatar/${user.image}" alt="">

<%--<button onclick="location.href = '/buy/${item.id}'" id="buyButton" class="float-left submit-button" >Buy</button>--%>

</body>
</html>
