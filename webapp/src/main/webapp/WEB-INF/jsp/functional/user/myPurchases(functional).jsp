<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--it needs for authorization (sec:authorize)-->
<%@ page session="false" %>
<html>
<head>
    <title>My Purchases</title>
</head>
<body>
<table>
    <tr>
        <td>Title</td>
        <td>Price</td>
        <td>Date</td>
        <td>Full name</td>
        <td>Address</td>
        <td>comment</td>
    </tr>
    <%--${getAllItems}--%>
    <c:forEach items="${purchases}" var="purchase">
        <tr>
            <td><c:out value="${purchase.item.toyTitle}"/></td>
            <td><c:out value="${purchase.price}"/></td>
            <td><c:out value="${purchase.date}"/></td>
            <td><c:out value="${purchase.fullName}"/></td>
            <td><c:out value="${purchase.address}"/></td>
            <td><c:out value="${purchase.comment}"/></td>


                <%--<td><button onclick="location.href = '/profile/view/${user.username}'" id="Show" class="float-left submit-button" >Show</button></td>--%>
            <br>
        </tr>
    </c:forEach>
</table>
</body>
</html>
