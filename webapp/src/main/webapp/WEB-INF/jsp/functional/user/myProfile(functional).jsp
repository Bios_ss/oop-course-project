<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--it needs for authorization (sec:authorize)-->
<%@ page session="false"%>
<html>
<head>
    <title>Home</title>
    <style type="text/css">
        span.error {
            color: red;
        }
    </style>
    <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jQuery/jquery-1.11.0.min.js"></script>

</head>
<body>
<%--Add comment script--%>

<h1>Spring Security - Hello world!</h1>


<sec:authorize access="isAuthenticated()">
    <p>Your username: <sec:authentication property="principal.username" /></p>
    <p><a class="btn btn-lg btn-danger" href="<c:url value="/logout" />" role="button">Logout</a></p>
</sec:authorize>
<br>
<hr>

<a href="/profile/my-profile/edit">Edit</a><br>
<a href="/profile/my-profile/change-password">Change password</a><br>

<%--${getAllItems}--%>
id:	          <c:out value="${user.id}"/><br>
username:     <c:out value="${user.username}"/><br>
firstName:    <c:out value="${user.firstName}"/><br>
lastName:	  <c:out value="${user.lastName}"/><br>
email:	      <c:out value="${user.email}"/><br>
<img src="${pageContext.request.contextPath}/resources/images/avatar/${user.image}" alt="">

<br>
<a href="/profile/my-purchases">My purchases</a>
<%--<button onclick="location.href = '/buy/${item.id}'" id="buyButton" class="float-left submit-button" >Buy</button>--%>

</body>
</html>
