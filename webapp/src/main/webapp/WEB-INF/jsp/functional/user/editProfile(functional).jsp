<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<html>
<head>
    <title>AddItem</title>
    <style type="text/css">
        span.error {
            color: red;
        }
    </style>
</head>
<body>
<h1>Edit profile</h1>

<form:form method="post" commandName="editProfileForm" enctype="multipart/form-data">
    <table>
        <tr>
            <td>firstName:</td>
            <td><form:input path="firstName" /></td>
            <td><span class="error"><form:errors path="firstName" /></span></td>
        </tr>

        <tr>
            <td>lastName:</td>
            <td><form:input path="lastName" /></td>
            <td><span class="error"><form:errors path="lastName" /></span></td>
        </tr>

        <tr>
            <input type="file" name="file"/>
        </tr>

        <tr>
            <td colspan="3"><input type="submit" value="Submit" /></td>
        </tr>
    </table>
</form:form>

</body>
</html>