<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<html>
<head>
    <title>AddItem</title>
    <style type="text/css">
        span.error {
            color: red;
        }
    </style>
</head>
<body>
<h1>Registration</h1>

<form:form method="post" commandName="registrationForm" enctype="multipart/form-data">
    <table>
        <tr>
            <td>Username:</td>
            <td><form:input path="username" /></td>
            <td><span class="error"><form:errors path="username" /></span></td>
        </tr>

        <tr>
            <td>firstName:</td>
            <td><form:input path="firstName" /></td>
            <td><span class="error"><form:errors path="firstName" /></span></td>
        </tr>

        <tr>
            <td>lastName:</td>
            <td><form:input path="lastName" /></td>
            <td><span class="error"><form:errors path="lastName" /></span></td>
        </tr>

        <tr>
            <td>email:</td>
            <td><form:input path="email" /></td>
            <td><span class="error"><form:errors path="email" /></span></td>
        </tr>

        <tr>
            <td>password:</td>
            <td><form:input path="password" /></td>
            <td><span class="error"><form:errors path="password" /></span></td>
        </tr>

        <tr>
            <td>Confirm password:</td>
            <td><form:input path="confirmPassword" /></td>
            <td><span class="error"><form:errors path="confirmPassword" /></span></td>
        </tr>

        <tr>
            <input type="file" name="file"/>
        </tr>

        <tr>
            <td colspan="3"><input type="submit" value="Submit" /></td>
        </tr>
    </table>
</form:form>

</body>
</html>