<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<html>
<head>
    <title>AddItem</title>
    <style type="text/css">
        span.error {
            color: red;
        }
    </style>
</head>
<body>
<h1>Sign Up</h1>

<form:form method="post" commandName="itemForm" enctype="multipart/form-data">
    <table>
        <tr>
            <td>toyTitle:</td>
            <td><form:input path="toyTitle" value="${item.toyTitle}"/></td>
            <td><span class="error"><form:errors path="toyTitle" /></span></td>
        </tr>

        <tr>
            <td>toyInfo:</td>
            <td><form:input path="toyInfo" value="${item.toyInfo}"/></td>
            <td><span class="error"><form:errors path="toyInfo" /></span></td>
        </tr>

        <tr>
            <input type="file" name="file"/>
        </tr>

        <tr>
            <td>animalInfo:</td>
            <td><form:input path="animalInfo" value="${item.animalInfo}"/></td>
            <td><span class="error"><form:errors
                    path="animalInfo" /></span></td>
        </tr>

        <tr>
            <td colspan="3"><input type="submit" value="Submit" /></td>
        </tr>
    </table>
</form:form>

<a href="${pageContext.request.contextPath}/" title="Home">Home</a>
</body>
</html>