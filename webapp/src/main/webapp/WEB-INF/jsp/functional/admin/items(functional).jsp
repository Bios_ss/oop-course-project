<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--it needs for authorization (sec:authorize)-->
<%@ page session="false" %>
<html>
<head>
    <title>Home</title>
</head>
<body>
<%--Delete item script--%>
<script>
    $(document).ready(function () {
        var deleteLink = $("a:contains('Delete')");
        $(deleteLink).click(function (event) {
            var confirmBox = confirm("Are you sure ?");
            if (confirmBox) {
                $.ajax({
                    url: $(event.target).attr("href"),
                    type: 'DELETE',

                    success: function () {
                        confirm("+1");
                    },
                    error: function () {
                        alert(error);
                    }

                });
            } else {
                event.preventDefault();
            }
            event.preventDefault();
            setTimeout(window.location.replace("../admin/"), 1000);

        });
    });
</script>
<%--Delete item script end--%>
<table>
    <tr>
        <td>id</td>
        <td>Title</td>
        <td>Date</td>
        <td>Open this page</td>
        <td>Edit</td>
        <td>Show sales</td>
        <td>Delete</td>
    </tr>
    <%--${getAllItems}--%>
    <c:forEach items="${items}" var="item">
        <tr>
            <td><c:out value="${item.id}"/></td>
            <td><c:out value="${item.toyTitle}"/></td>
            <td><c:out value="${item.dateOfPost}"/></td>

            <td><a href="/item/${item.id}"> Show more..</a></td>

                <%--<td><button onclick="location.href = '/profile/view/${user.username}'" id="Show" class="float-left submit-button" >Show</button></td>--%>
            <td>
                <button onclick="location.href = '/admin/item/${item.id}/edit'" id="Edit"
                        class="float-left submit-button">Edit
                </button>
            </td>
            <td>
                <button onclick="location.href = '/admin/purchases/item/${item.id}'" id="Show_sales"
                        class="float-left submit-button">Show sales
                </button>
            </td>

            <td>
                <a href="../../admin/item/${item.id}/delete">Delete</a>
            </td>

            <br>
        </tr>
    </c:forEach>
</table>
</body>
</html>
