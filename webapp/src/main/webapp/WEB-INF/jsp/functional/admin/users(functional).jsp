<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--it needs for authorization (sec:authorize)-->
<%@ page session="false" %>
<html>
<head>
    <title>Home</title>
</head>
<body>


<%--${getAllItems}--%>
<c:forEach items="${users}" var="user">
    id:	          <c:out value="${user.id}"/><br>
    username:     <c:out value="${user.username}"/><br>
    firstName:    <c:out value="${user.firstName}"/><br>
    lastName:	  <c:out value="${user.lastName}"/><br>
    email:	      <c:out value="${user.email}"/><br>
    <button onclick="location.href = '/profile/view/${user.username}'" id="Show" class="float-left submit-button" >Show</button><br>
    <button onclick="location.href = '/admin/user/${user.username}/edit'" id="Edit" class="float-left submit-button" >Edit</button><br>
    <br>
    <hr>
</c:forEach>

</body>
</html>
