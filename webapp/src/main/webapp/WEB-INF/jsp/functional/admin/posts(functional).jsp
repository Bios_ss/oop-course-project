<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--it needs for authorization (sec:authorize)-->
<%@ page session="false" %>
<html>
<head>
    <title>Home</title>
</head>
<body>
<%--Delete post script--%>
<script>
    $(document).ready(function() {
        var deleteLink = $("a:contains('Delete')");
        $(deleteLink).click(function(event) {
            var confirmBox = confirm("Are you sure ?");
            if(confirmBox){
                $.ajax({
                    url: $(event.target).attr("href"),
                    type: 'DELETE',

                    success: function() {
                        confirm("+1");
                    },
                    error: function(){
                        alert(error);
                    }

                });
            } else {
                event.preventDefault();
            }
            event.preventDefault();
            setTimeout(window.location.replace("../admin/"), 1000);

        });
    });
</script>
<%--Delete post script end--%>
<table>
    <tr>
        <td>id</td>
        <td>Title</td>
        <td>Date</td>
        <td>Open this page</td>
        <td>Edit</td>
        <td>Delete</td>
    </tr>
    <%--${getAllItems}--%>
    <c:forEach items="${posts}" var="post">
        <tr>
            <td><c:out value="${post.id}"/><br></td>
            <td><c:out value="${post.title}"/><br></td>
            <%--<td><c:out value="${post.info}"/><br></td>--%>
            <td><c:out value="${post.dateOfPost}"/><br></td>

            <td><a href="/post/${post.id}"> Show more..</a></td>

                <%--<td><button onclick="location.href = '/profile/view/${user.username}'" id="Show" class="float-left submit-button" >Show</button></td>--%>
            <td>
                <button onclick="location.href = '/admin/post/${post.id}/edit'" id="showSales" class="float-left submit-button">Edit</button>
            </td>
            <td><a href="../../admin/post/${getPost.id}/delete">Delete</a></td>
            <br>
        </tr>
    </c:forEach>
</table>
</body>
</html>
